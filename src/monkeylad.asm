/*
Monkey Lad (c) by Eric Fry, surt, niloct

Monkey Lad is licensed under a
Creative Commons Attribution-NonCommercial 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-nc/4.0/>.
*/

.memorymap
defaultslot 0
slotsize $4000
slot 0 $0000
slotsize $4000
slot 1 $4000
slotsize $4000
slot 2 $8000
slotsize $2000
slot 3 $c000 ; memory
.endme

.rombankmap
bankstotal 9
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
banksize $4000
banks 1
.endro

.sdsctag 1.1, "Monkey Lad","Monkey Lad","Eric Fry"

.define BANK_SWITCH $ffff
.define VDPStatusPort $bf
.define VDPDataPort $be

.define TILEMAP_START_ADDRESS $3800

.define HUD_TILE_SPRITE_OFFSET $2000 + 8 * 32
.define MONKEY_WINGS_TILE_START_OFFSET HUD_TILE_SPRITE_OFFSET + 24 * 32
.define MONSTER_TILE_START_OFFSET MONKEY_WINGS_TILE_START_OFFSET + 4 * 32

; control pad button maps
.define P1_BUTTON_RIGHT %00001000
.define P1_BUTTON_LEFT  %00000100
.define P1_BUTTON_DOWN  %00000010
.define P1_BUTTON_UP    %00000001
.define P1_BUTTON_A     %00010000
.define P1_BUTTON_B     %00100000

.define LEFT_SIDE $ff ; -1
.define RIGHT_SIDE 1

.define PLAYER_WIDTH_IN_TILES  2
.define PLAYER_HEIGHT_IN_TILES 3

.define PLAYER_SCREEN_CENTRE_X 120
.define PLAYER_SCREEN_CENTRE_Y 104
.define PLAYER_SCREEN_Y_FALL_THRESHOLD 120

.define ACTION_STILL      0
.define ACTION_SKID       1
.define ACTION_WALK       2
.define ACTION_RUN        3
.define ACTION_IN_THE_AIR 4
.define ACTION_DEAD       5

.define LAD_SPRITE_COUNT        10 ; sprites per direction
.define LAD_SPRITE_WALK_OFFSET  2
.define LAD_SPRITE_SKID_OFFSET  7
.define LAD_SPRITE_ANGEL_OFFSET 19
.define PLAYER_DIR_LEFT        LAD_SPRITE_COUNT
.define PLAYER_DIR_RIGHT       0

.define LEFT_WING_SPRITE 32
.define RIGHT_WING_SPRITE 34

.define MAX_WALK_VELOCITY       $280 ; --$0380
.define WALK_ACCELERATION         $E
.define WALK_DECELERATION         $E
.define SKID_DECELERATION        $20
.define INITIAL_WALK_VELOCITY    $90
.define MAX_DOWNWARD_VELOCITY   $450
.define WALL_JUMP_X_VELOCITY    $300
.define MAX_WALL_SLIDE_VELOCITY $100
.define GRAVITY_ACCELERATION     $50

.define STOMP_ATTACK_VELOCITY   $400

.define ANGEL_RISE_VELOCITY     $10E

.define WALL_SLIDE_LEFT  $ff ; -1
.define WALL_SLIDE_RIGHT   1

.define MAX_PLAYER_Y 40 * 8 + PLAYER_SCREEN_CENTRE_Y ;FIXME this is a hack for testing.
.define SCREEN_WIDTH 32 * 8
.define SCREEN_HEIGHT 24 * 8

.define SOLID_TOP_FLAG 0
.define SOLID_BOTTOM_FLAG 1
.define SOLID_LEFT_FLAG 2
.define SOLID_RIGHT_FLAG 3
.define BLOCK_UPDATES_CAMERA_FLAG 4
.define BLOCK_CAUSES_DAMAGE 5

.define SAT_START_OFFSET 3 ; skip the three sprites that make up the monkey lad face picture + life counter.

.define PADDING 0

.define TILE_BITFLAG_UPDATES_CAMERA_FLAG 5 ;%00100000
.define TILE_BITFLAG_FORCE_PASSABLE      6 ;%01000000
.define TILE_BITFLAG_UNUSED              7 ;%10000000

.define FORCE_PASSABLE_BITMASK %11110000

; object status flags
.define OBJ_FLAG_ACTIVE       1
.define OBJ_FLAG_DONT_RESPAWN 2
.define OBJ_FLAG_DIR_LEFT     4
.define OBJ_FLAG_DIR_RIGHT    8
.define OBJ_FLAG_STATIC      16
.define OBJ_FLAG_LONG        32  ; 3x2 tiles instead of 2x2

.define OBJ_FLAG_DIR_CLEAR_MASK %11110011

.define OBJ_BITFLAG_ACTIVE       0
.define OBJ_BITFLAG_DONT_RESPAWN 1
.define OBJ_BITFLAG_DIR_LEFT     2
.define OBJ_BITFLAG_DIR_RIGHT    3
.define OBJ_BITFLAG_STATIC       4
.define OBJ_BITFLAG_LONG         5

.define OBJECT_INITIAL_FLAGS OBJ_FLAG_ACTIVE
.define OBJ_NONE         0
.define OBJ_SNAIL        1
.define OBJ_BIRD         2
.define OBJ_JUMPING_FISH 3
.define OBJ_LEVEL_END    4
.define OBJ_NEW_LIFE     5

.define MONSTER_BASE_WIDTH 16
.define MONSTER_HEIGHT     16
.define PLAYER_WIDTH       16
.define PLAYER_HEIGHT      24

.define MAX_ITEM_BLOCKS 32

.struct levelInfo_struct
   mapWidth dw
   mapHeight dw
   mapWidthInBlocks db
   maxPlayerX dw
   numItemTiles db
   numItemBlocks db
   gfxAssetsBank db
   palAddress dw
   numPalEntries db
   tilesAddress dw
   titleDataLength dw
   monsterTilesBank db
   monsterTilesAddress dw
   monsterTitleDataLength dw
   blockMapBank db
   blockMapAddress dw
   blockDataBank db ; bank containing block data. 8x8 tile chunks
   startingTileX dw
   startingTileY dw
   playerStartingTileX dw 
   playerStartingTileY dw
   musicBank db
   musicAddress dw
   hitFlagsBank db
   hitFlagsAddress dw
   objectDataBank db
   objectDataAddress dw
.endst

.struct player_struct
   x dw
   y dw
   oldX dw
   xSubPixel dw
   ySubPixel dw
   vx dw
   vy dw
   currentFrame db
   oldFrame db
   dir db
   action db
   wallSliding db   ; 0 = not sliding, -1 = slide left, 1 = slide right
   hasChangedFrame db
   isDead db
   lifeCounter db
   currentHitFlags db
.endst

.struct object
   type db
   flags db
   x dw
   y dw
   data dw
   counter dw
  curTile db
      
.endst

.ramsection "RAM" slot 3
    pauseFlag db 
    vblankFlag db 
    gameFlags db
    randSeed dw
    randValue dw
    lastInputState db
    buttonAPressed db
    buttonAHeld db
    isPalTv db ; set to 1 for PAL, 0 for NTSC
    frameRate db ; either 50 for PAL or 60 for NTSC
    frameCounter db
    elapsedTime dsb 3 ; bcd in seconds
    

    ;map control data
    curLevel db
    curLevelInfo instanceof levelInfo_struct
    nextLevel db ; number of next level to go to.
    ;Coords of the top left corner of the screen. In pixels.
    mapX dw
    mapY dw
    newMapY dw      ; the new camera Y position. This is updated by landing on a tile that resets the camera.
    maxMapX dw      ; this is level width - screen width (in pixels)
    maxMapY dw      ; this is level height - screen height (in pixels)
    vdpXScrollReg db
    vdpYScrollReg db
    fineScrollX db
    fineScrollY db
    screenXPos db  ; starting horizontal offset for the screen on the tilemap.
    screenYPos db  ; starting vertical offset for the screen on the tilemap.
    newTileYPos db ; the row to write the new tiles to. (0 .. 27)
    newTileXPos db ; the column to write the tiles to. (0 .. 31)
    screenStartTileXPos db ; start of map tiles shown on the screen. (top left corner)
    screenStartTileYPos db ; start of map tiles shown on the screen. (top left corner)
    newRowTiles dsb 64 ; 32 tiles
    newColumnTiles dsb 50 ; 25 tiles
    mapCmdFlags db ; bit flags to indicate if new tiles should be added to the map.
    drawNewColumn db ; flag set if we need to draw a new column of tiles to the background.
    drawNewRow db ; flag set if we need to draw a new row of tiles to the background.
    xScrollDir db ; 1 for scrolling right -1 for scrolling left, 0 no vertical scroll
    yScrollDir db ; 1 for scrolling down -1 for scrolling up, 0 no vertical scroll
    lastColumnAdded db ; which side of the screen was the last column added to. 0 no column added. -1 Left column. 1 Right column.
    
    SpriteTableNeedsUpdate db   ; bool flag to indicate that we should reload the sprite table.
    SpriteStartOffset db        ; start of active sprites. after the HUD sprites.
    SpriteCount db ; number of active sprite entries.
    SpriteAttributeTableY dsb 64
    SpriteAttributeTableXN dsw 64
    
    player instanceof player_struct

    objects instanceof object 32
    objectsTotal db
    objectsOnScreenCount db
    objectsOnScreenPtr dsw 32
    
    curObjectPtr dw ; a pointer to the currently accessed object data.
    curTileNumber dw ; used by player collision logic. This is the maptile currently being checked.
    curBlockNumber dw ; a place to store the current block number
    
    itemBlocks dsw MAX_ITEM_BLOCKS
.ends

.define FLAG_MUSIC_PLAYING     1
.define FLAG_RETURN_TO_TITLE_SCREEN 2
.define FLAG_GAME_OVER 4
.define FLAG_NEXT_LEVEL 8

.define BITFLAG_MUSIC_PLAYING     0
.define BITFLAG_RETURN_TO_TITLE_SCREEN 1
.define BITFLAG_GAME_OVER 2
.define BITFLAG_NEXT_LEVEL 3

.include "macros.inc"

.bank 0 slot 0

.org $0000
.section "boot" force
    di
    im 1
    jp main
.ends

;Interrupt handler
.org $38
    push af
    in a, (VDPStatusPort) ; acknowledge interrupt
    ld a, 1
    ld (vblankFlag), a
    pop af
    ei
reti

;Pause button NMI
.org $66
   push af
   ld a, (pauseFlag)
   xor 1 
   ld (pauseFlag), a
   cp 1
   jp z, +
   ;call PSGMOD_Start
   jp ++
+:
   ;call PSGMOD_Pause
++:
   pop af
retn

.bank 0 slot 0
.section "all" force

.include "video.inc"
.include "maths.inc"

main:
   ld sp, $dff0
   xor a

   call PSGInit
  
   call DetectTVType
   ld (isPalTv), a
   ld b, 60 ; ntsc tv type
   or a
   jp z, +
   ld b, 50 ; pal tv type
+:
   ld a, b
   ld (frameRate), a

   call SetupVDP
  
   ld   a, :TitleMusic
   ld   hl, TitleMusic
   call PSGPlayNoRepeat
     
   ld a, 0
   ld (pauseFlag), a
   ld (vblankFlag), a
   
   ld (gameFlags), a
   or FLAG_MUSIC_PLAYING
   ld (gameFlags), a
 
;-------

   ld hl,TitlePal
   ld b,(TitlePalEnd-TitlePal)
   call LoadPalette

   ld a,   :TitleTileData
   ld (BANK_SWITCH), a
   ld hl,TitleTileData              ; Location of tile data
   ld bc,TitleTileDataEnd-TitleTileData  ; Counter for number of bytes to write
   call LoadTiles

   ld hl,TitleTilemap
   ld bc,TitleTilemapEnd-TitleTilemap; Counter for number of bytes to write
   call LoadTilemap

;-------

   call ScreenOn

   ei

; title loop
-:
   call WaitVblank

   call PSGFrame

   ; any keypress on controller 1 or pause will start the game.
   in a, ($dc)
   xor $ff
   ld b, a
   ld a,(pauseFlag)
   or b
   jp z,-

loadGame:
   ; clear player struct
   ld b, _sizeof_player_struct
   ld hl, player
   call ClearRAM
   
   ld a, 3
   ld (player.lifeCounter), a
   
   ld a, 0    ;Load first level data
   ld (nextLevel), a
   
resetLevel:
   call ScreenOff
   call PSGStop
   di
   call ClearVRAM

   call ScreenOff
   di

   call HideColumnZero
   
   call LoadSpritePalette
   
   ld a, (nextLevel) ; level number
   call LoadLevel
   ld a, 0
   ld (SpriteCount), a
   call HUDInit
   call PlayerInit

   
   xor a
   ld bc, 0
   ld (gameFlags), a
       
   xor a
   ei
   ld (pauseFlag), a

   ld a, (frameRate)
   ld (frameCounter), a
   ld hl, 0
   ld (elapsedTime), hl

   ld a, $ff
   ld (lastInputState), a
  
   call ScreenOn

   xor a
   ld (vblankFlag), a
  
   xor a
   or FLAG_MUSIC_PLAYING
   ld (gameFlags), a

    
   ld  hl, (curLevelInfo.musicAddress)
   call PSGPlay
     
gameloop:
   call WaitVblank
   call ScreenOff
   call UpdateVdpScroll
   ld a, (player.hasChangedFrame)
   dec a
   jp nz, +
   ld (player.hasChangedFrame), a ; reset flag
   ld a, (player.currentFrame)
   ld d, a
   call PlayerUpdateSpriteTiles
+:
   call SpritesWriteToVRAM

   call ScreenOn
   
   ld a, (frameCounter)
   and %111
   cp 4
   jp nz, +
;begin  --  frameCounter % 4 == 0
   call ObjectsActivate
;endif
+:

   call ObjectsUpdate
   
   ld a, (player.action)
   cp ACTION_DEAD
   jp nz, +
;begin  --  player is dead
   PlayerUpdateY
   ld bc, (mapY)
   ld hl, (player.y)
   ld de, 24
   add hl, de
   xor a
   sbc hl, bc
   ld a, h
   or a
   jp m, ExitFromLevel ; exit from level when the player leaves the top of the screen.
      
   jp endOfInputProcessing
;endif
+:
   call ReadInput

   ld a, (lastInputState) ; check A button state.
   and P1_BUTTON_A
   jp nz, ++
;begin  --  button a pressed
   inc a
   ld (buttonAPressed), a
   jp +
;else
++:
   xor a
   ld (buttonAPressed), a
   ld (buttonAHeld), a
;endif
+:
  
   or a
   jp nz, ++
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp nz, +
;begin  --  buttonAPressed or player.action == ACTION_IN_THE_AIR
++:
   call PlayerJumpLogic
   jp ++
;endif
+:
   
   ld a, (lastInputState)
   ld b, a
   and P1_BUTTON_RIGHT
   jp nz,+ ; button not pressed so ignore.
      ld b, 3
      ld a, (lastInputState)
      and P1_BUTTON_A
      ld c, 0
      jp nz, +++
      ld c, -1
+++:
      ld a, (lastInputState)
      and P1_BUTTON_B
      jp nz, +++
      ld c, 1
+++:
      ;call ScrollMap
      call PlayerMoveRight
      
      jp ++
+:
   ld a, b
   and P1_BUTTON_LEFT
   jp nz,+ ; button not pressed so ignore.
      call PlayerMoveLeft
      jp ++
;+:
;   ld a, b
;   and P1_BUTTON_UP
;   jp nz,+ ; button not pressed so ignore.
;      ld b, 0
;      ld c, -3
;      ;call ScrollMap
;      jp ++
+:
   ld a, b
   and P1_BUTTON_DOWN
   jp nz,+ ; button not pressed so ignore.
      ld b, 0
      ld c, 1
      ;call ScrollMap
+:
   call PlayerNoDirectionPressed
++:

   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp nz, +
;begin  --  player.action == ACTION_IN_THE_AIR

   PlayerUpdateY

   ld hl, (player.y)
   ld bc, -(MAX_PLAYER_Y+1) ;FIXME this should be calculated based on the level dimensions
   add hl, bc
   ld a, h
   or a
   jp m, ++
   ;begin  --  player.y > MAX_PLAYER_Y
      ; player hit bottom of map. Change action to walking
      ld hl, MAX_PLAYER_Y
      ld (player.y), hl
      ld a, ACTION_WALK
      ld (player.action), a
      call PlayerDead
   ;endif
   ++:
;endif
+:

   call PlayerCheckMapCollision
   call PlayerCheckForEnemyCollision
   call PlayerUpdate
   call CameraUpdate
   
endOfInputProcessing:

   ;update sprites
   xor a
   ld (SpriteCount), a
   call PlayerUpdateSprite
   call ObjectsUpdateSprites
   ; call Update Monsters here.
   
   ld  a, (curLevelInfo.musicBank)
   ld (BANK_SWITCH), a
   call PSGFrame
   ld a, :SfxData
   ld (BANK_SWITCH), a   
   call PSGSFXFrame
   
   ld hl, frameCounter
   inc (hl)
   
   ld a, (gameFlags)
   and FLAG_NEXT_LEVEL
   jp z, gameloop
   ;proceed to the next level
   
   ;FIXME show level end screen here. :)
   jp resetLevel

WaitVblank:
   ld a, (vblankFlag)
   cp 1
   jp z, +
   
   halt
   +:
   xor a
   ld (vblankFlag), a
ret

  
PauseMusic:
   push af
   ld a, 1
   ld (pauseFlag), a
   pop af
ret

WaitForKeyPress:
   push af
-:
   call WaitVblank
   call PSGFrame
   ; any keypress on controller 1 
   in a, ($dc)
   xor $ff
   or a
   jp z,-
   pop af
   ret

WaitForAllKeysUp:
   push af
-:
   call WaitVblank
  call PSGFrame
   ; any keypress on controller 1 
   in a, ($dc)
   xor $ff
   or a
   jp nz,-
   pop af
   ret

; b=number of frames to wait
;trashes a
WaitForNFrames:
  -:
  push bc
  call WaitVblank
  call PSGFrame
  pop bc
  dec b
  ld a, b
  or a
  jp nz, -
  
ret

LoadSpritePalette:
   ld a,   :SpritePalette
   ld (BANK_SWITCH), a
   
   ld hl,SpritePalette
   ld b,(SpritePaletteEnd-SpritePalette)
   ld c, 16 ; sprite palette
   call LoadPaletteWithOffset
   
   ld a, 0
   call SetOverscanBGColour
ret


; a=level number
LoadLevel:
   ;Load level data into ram for easy access.
   ld h, a
   ld e, _sizeof_levelInfo_struct
   call mul
   ld bc, LevelData
   add hl, bc
   ld de, curLevelInfo
   ld bc, _sizeof_levelInfo_struct
   
   ldir
   
   ld a, (curLevelInfo.gfxAssetsBank)
   ld (BANK_SWITCH), a
    
   ld hl, (curLevelInfo.palAddress)
   ld a, (curLevelInfo.numPalEntries)
   ld b, a
   call LoadPalette
   
   ld bc, (curLevelInfo.titleDataLength)  ; Counter for number of bytes to write
   ld hl, (curLevelInfo.tilesAddress)     ; Location of tile data

   call LoadTiles
   
   ld a, (curLevelInfo.monsterTilesBank)
   ld (BANK_SWITCH), a
   ld bc, (curLevelInfo.monsterTitleDataLength)
   ld hl, (curLevelInfo.monsterTilesAddress)
   ld de, MONSTER_TILE_START_OFFSET
   call LoadTilesToAddress

;   ld a, (curLevelInfo.startingBlockX)
;   ld d, a
;   ld a, (curLevelInfo.startingBlockY)
;   ld e, a
;   call LoadInitialMap
   ld hl, (curLevelInfo.startingTileX)
   ld b, h
   ld c, l
   ld hl, (curLevelInfo.startingTileY)
   ld d, h
   ld e, l
   call WriteMapToTilemap
   
   ; load starting map coords for the screen.
   ld hl, (curLevelInfo.startingTileX)
   HLx8
   ld bc, mapX
   ld a, l
   ld (bc), a
   inc bc
   ld a, h
   ld (bc), a
   
   ld hl, (curLevelInfo.startingTileY)
   HLx8
   ld bc, mapY
   ld de, newMapY
   ld a, l
   ld (bc), a
   ld (de), a
   inc bc
   inc de
   ld a, h
   ld (bc), a
   ld (de), a   

   ld a, 7
   ld (vdpXScrollReg), a
   ld (fineScrollX), a 
   out (VDPStatusPort),a
   ld a,$88 ;FIXME define here
   out (VDPStatusPort),a

   ; calculate maxMapX
   ld hl, (curLevelInfo.mapWidth)
   HLx8
   ld bc, -(SCREEN_WIDTH)
   add hl, bc
   ld (maxMapX), hl
   
   ; calculate maxMapY
   ld hl, (curLevelInfo.mapHeight)
   HLx8
   ld bc, -(SCREEN_HEIGHT)
   add hl, bc
   ld (maxMapY), hl
   
   xor a

   ld (lastColumnAdded), a
   ld (screenXPos), a
      

   ld (vdpYScrollReg), a

   ld (fineScrollY), a

   ld (screenYPos), a
   ld (newTileYPos), a
   ld (newTileXPos), a
   ld (drawNewColumn), a
   ld (yScrollDir), a
   ld (xScrollDir), a
   ld (drawNewRow), a
   ld (drawNewColumn), a
   ld (screenStartTileXPos), a
   ld (screenStartTileYPos), a

   call LoadObjects
   
   ld b, MAX_ITEM_BLOCKS * 2
   ld hl, itemBlocks
   call ClearRAM
ret

PlayerInit:

   
   ;load initial player x coord. mapX + playerStartingScreenX
   ld hl, (mapX)
   ld b, h
   ld c, l
   
   ld hl, (curLevelInfo.playerStartingTileX)
   add hl, bc
   ld (player.x), hl
   
   ;load initial player y coord. mapY + playerStartingScreenY
   ld hl, (mapY)
   ld b, h
   ld c, l
   
   ld hl, (curLevelInfo.playerStartingTileY)
   add hl, bc
   ld (player.y), hl
   
   
   ld a, 3 ; FIXME 
   ld (player.currentFrame), a
   
   call PlayerInitSpriteTiles
   
   ld d, 3
   call PlayerUpdateSpriteTiles
   
   call PlayerUpdateSprite

   ;Load monkey lad angel wings
   ld de, MONKEY_WINGS_TILE_START_OFFSET
   ld hl, MonkeyWingsTileData
   ld bc, MonkeyWingsTileDataEnd - MonkeyWingsTileData
   
   ld a, :MonkeyWingsTileData
   ld (BANK_SWITCH), a
   call LoadTilesToAddress
   
ret

HUDInit:
   ld de, HUD_TILE_SPRITE_OFFSET
   ld hl, HUDTileData
   ld bc, HUDTileDataEnd - HUDTileData
   
   ld a, :HUDTileData
   ld (BANK_SWITCH), a
   call LoadTilesToAddress
   
   ld b, 0
   ld c, 8
   ld h, 9
   ld l, 14
   call InitSprite
   
   ld b, 1
   ld c, 10
   ld h, 9 + 8
   ld l, 14
   call InitSprite

   call HUDUpdate
   
   ld a, SAT_START_OFFSET
   ld (SpriteStartOffset), a

ret

HUDUpdate:
   ; FIXME Need to handle double digit life here. Also need to handle adding extra lives though the level.
   ld b, 2
   ld c, 12
   ld a, (player.lifeCounter)
   add a, a
   add a, c
   ld c, a
   ld h, 9 + 8 + 10
   ld l, 14
   call InitSprite
ret

; scroll the map 
;
; b = signed x direction
; c = signed y direction
; 
ScrollMap:
   ;update mapX/Y
   ld hl, (mapX)
   ld e, b
   SignExtend_DE
   add hl, de
   ld (mapX), hl
   
   ld a, c
   or a
   jp z, + ;don't bother updating mapY if y == 0
   
   ld hl, (mapY) ; mapY = mapY + c
   ld e, c
   SignExtend_DE
   add hl, de
   ld (mapY), hl
+:
   ld a, b
   or a
   jp z, scrollVertical ; no x movement so skip to vertical.
   jp p, scrollRight ; x > 0 so moving right
    
scrollLeft:
   neg
   ld b, a
   ld a, -1
   ld (xScrollDir), a
   ld a, (fineScrollX)
   add a, b
   cp 8
   jp c, +
   ; greater than or equal to 8
   and %111 
   ld (fineScrollX), a ; fineScrollX = (fineScrollX + b) % 8
   ld a, (screenXPos)
   inc a ; screenXPos++
   ld e, a
   or a
   jp z, +++
   neg
   add a, 32 ; newTileXPos = 32 - screenXPos
+++:
;   dec a
;   jp p, +++
;   ld a, 31
;+++:
   ld (newTileXPos), a
   ld a, e
   cp 32 ;wrap around tilemap to beginning if required.
   jp nz, ++
      xor a
   jp ++

scrollRight:
   neg
   ld b, a
   ld a, 1
   ld (xScrollDir), a
   ld a, (fineScrollX)
   add a, b
   or a
   jp p, +
   ; a < 0
   add a, 8
   ld (fineScrollX), a ; fineScrollX = 8 - (fineScrollX - b)
   ld a, (screenXPos)
   ld e, a
   or a
   jp z, +++
   neg
   add a, 32 ; newTileXPos = 32 - screenXPos
+++:
;   inc a
;   cp 32
;   jp nz, +++
;   xor a
;+++:   
   ld (newTileXPos), a
   ld a, e
   dec a ; screenXPos--
   jp p, ++
      ld a, 31

; update screen by whole tile
++:
   ld (screenXPos), a
   ; pack scroll and fine scroll into reg form. (screenXPos << 3 + fineScrollX)
   add a, a
   add a, a
   add a, a
   ld e, a
   ld a, (fineScrollX)
   add a, e
   ld (vdpXScrollReg), a

   ld a, 1
   ld (drawNewColumn), a ; set the drawNewColumn flag
   
   jp scrollVertical

; update just the fineScrollX part.
+:
   ld (fineScrollX), a
   ld a, (screenXPos)
   ; pack scroll and fine scroll into reg form. (screenXPos << 3 + fineScrollX)
   add a, a
   add a, a
   add a, a
   ld e, a
   ld a, (fineScrollX)
   add a, e
   ld (vdpXScrollReg), a 

   
scrollVertical:
   ld a, 0
   ld (yScrollDir), a
   ld a, c
   or a
   jp z, scrollGatherNewMapTiles; no y movement jump to gather new tiles logic.
   jp p, scrollDown ; y > 0 so moving down
   
scrollUp:
   ld a, -1
   ld (yScrollDir), a
   ld a, (fineScrollY)
   add a, c
   or a
   jp p, +
   ; a < 0
   add a, 8
   ld (fineScrollY), a ; fineScrollY = 8 - (fineScrollY - c)
   ld a, (screenYPos)
   dec a ; screenYPos--
   jp p, +++
      ld a, 27 ; wrap if a < 0
+++:
   ld (newTileYPos), a

   jp ++

scrollDown:
   ld a, 1
   ld (yScrollDir), a
   ld a, (fineScrollY)
   add a, c
   cp 8
   jp c, +
   ; greater than or equal to 8
   and %111 
   ld (fineScrollY), a ; fineScrollY = (fineScrollY + c) % 8
   ld a, (screenYPos)
   inc a ; screenYPos++
   cp 28
   jp c, +++
      xor a ; wrap if a >= 28
+++:
   ld e, a
   add a, 24
   cp 28
   jp c, +++
   ; greater than or equal to 28
   ;neg
   add a, -28 ; newTileXPos = (screenXPos + 1 + 24) - 28
+++:   
   ld (newTileYPos), a
   ld a, e

; update screen by whole tile
++:
   ld (screenYPos), a
   ; pack scroll and fine scroll into reg form. (screenYPos << 3 + fineScrollY)
   add a, a
   add a, a
   add a, a
   ld e, a
   ld a, (fineScrollY)
   add a, e 
   ld (vdpYScrollReg), a

   ld a, 1
   ld (drawNewRow), a ; set the drawNewRow flag
   
   jp scrollGatherNewMapTiles

; update just the fineScrollY part.
+:
   ld (fineScrollY), a
   ld a, (screenYPos)
   ; pack scroll and fine scroll into reg form. (screenYPos << 3 + fineScrollY)
   add a, a
   add a, a
   add a, a
   ld e, a
   ld a, (fineScrollY)
   add a, e 
   ld (vdpYScrollReg), a

scrollGatherNewMapTiles:
   ld a, (drawNewRow)
   dec a
   call z, scrollGatherNewRowMapTiles
   
   ld a, (drawNewColumn)
   dec a

   jp z, scrollGatherNewColMapTiles ; this is a function and will ret
   
   ;fall through and return
   ret

.macro writeEmptyTile
   ; write out empty tile
   ; writes empty tile to (de)
   ; increments hl and de by 2 
   inc hl
   inc hl
   xor a
   ld (de), a
   inc de
   ld (de), a
   inc de
.endm

scrollGatherNewRowMapTiles:
   ld a, (yScrollDir)
   or a
   jp p, + ; positive y direction so scrolling down.
   call GetMapBlock ; scroll up.
   jp ++
+:
   call GetMapBlockBottomLeft
++:   
   push de
   push bc
   call GetMapBlockNumber
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ;process item block here.
      ld (curBlockNumber), de
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      push bc
      ;a = (y * 8 + x) * 2
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, b
      ld c, a  ; c = tile index in the block
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      ld de, newRowTiles
-: ; do {
      push bc
      push de
      push hl
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +
      ;begin -- c >= 32   
         inc de
         sub 32
         ld c, a
      ;endif
+:
      ld hl, ItemblockBitmaskTbl
      ld b, 0
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      ;begin -- write out tile
         ldi
         ldi
         jp ++
      +:
      ;else
         writeEmptyTile     
      ;endif
      ++:
      
      pop bc
   
      inc b
      bit 3, b
      jp nz, + ;exit loop if b >= 8
      inc c
      jp -
      
   +:
      ld h, d
      ld l, e
      
      jp ++++
;else
+++:
      ;-- Not an item block
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;a = (c * 8 + b) * 2
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, b
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      ld de, newRowTiles
      ld a, b
      neg
      add a, 8
      add a, a
      push bc
      ld c, a ; c = (8 - b) * 2
      ld b, 0
      
      ldir ; copy c worth of data from (blockOffset: hl) to (newRowTiles: de)
      ld h, d
      ld l, e
;endif
++++:

.rept 3   
   pop bc
   pop de
   
   inc d
   
   push de
   push hl ; newTilePointer
   push bc
   call GetMapBlockNumber
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ;process item block here.
      ld (curBlockNumber), de
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block

      ;a = c * 8
      ld a, c
      add a, a
      add a, a
      add a, a

      ld h, 0
      ld l, a
      add hl, hl ; x 2 because tiles are words
      add hl, de
      pop de ; newTilesPointer
      push bc

      ld c, a ; y * 8 index in block
      ld b, 0 ; loop counter
-: ; do {
      push bc
      push de
      push hl
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +
      ;begin -- c >= 32   
         inc de
         sub 32
         ld c, a
      ;endif
+:
      ld hl, ItemblockBitmaskTbl
      ld b, 0
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      ;begin -- write out tile
         ldi
         ldi
         jp ++
      +:
      ;else
         writeEmptyTile     
      ;endif
      ++:
      
      pop bc
   
      inc b
      bit 3, b
      jp nz, + ;exit loop if b >= 8
      inc c
      jp -
      
   +:     
      jp ++++
;else
+++:   
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;a = c * 8 * 2
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      pop de ; newTilesPointer
      push bc
      ld bc, 16 ; c = 8 pixels * 2
      
      ldir ; copy c worth of data from (blockOffset: hl) to (newRowTiles: de)
;endif
++++:
   ld h, d
   ld l, e
.endr

   pop bc
   pop de   

   ld a, b
   or b
   ret z
   
   inc d
   
   push hl ; newTilePointer
   push bc
   call GetMapBlockNumber
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ;process item block here.
      ld (curBlockNumber), de
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block

      ;a = c * 8
      ld a, c
      add a, a
      add a, a
      add a, a

      ld h, 0
      ld l, a
      add hl, hl ; x 2 because tiles are words
      add hl, de
      pop de ; newTilesPointer

      ld c, a ; y * 8 index in block
      ld a, b
      neg
      add a, 8
      ld b, a ; loop counter

-: ; do {
      push bc
      push de
      push hl
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +
      ;begin -- c >= 32   
         inc de
         sub 32
         ld c, a
      ;endif
+:
      ld hl, ItemblockBitmaskTbl
      ld b, 0
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      ;begin -- write out tile
         ldi
         ldi
         jp ++
      +:
      ;else
         writeEmptyTile     
      ;endif
      ++:
      
      pop bc
   
      inc b
      bit 3, b
      jp nz, + ;exit loop if b >= 8
      inc c
      jp -
      
   +:       
      jp ++++
;else
+++:       
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;a = c * 8 * 2
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      pop de ; newTilesPointer
      sla b
      ld c, b ; c = b * 2 
      ld b, 0
      
      ldir ; copy c worth of data from (blockOffset: hl) to (newRowTiles: de)
;endif
++++:
   ret

scrollGatherNewColMapTiles:
   ld a, (xScrollDir)
   or a
   jp p, + ; positive x direction so scrolling right.
   call GetMapBlock ; scroll left.
   ld a, LEFT_SIDE
   ld (lastColumnAdded), a
   jp ++
+:
   call GetMapBlockTopRight
   ld a, RIGHT_SIDE
   ld (lastColumnAdded), a
++:

   ;set the x position for the start of the screen. (top left corner)
   or a ; lastColumnAdded
   jp m, +
   jp z, +
   ld a, (newTileXPos) ; scrolling right so shift start one tile right so end tile is on newTileXPos.
   inc a
   cp 32
   jp nz, ++
   ld a, 0 ; write back to zero if required
   jp ++
+:
   ld a, (newTileXPos)
++:
   ld (screenStartTileXPos), a
   
   push de
   push bc
   call GetMapBlockNumber
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ;process item block here.
      ld (curBlockNumber), de
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      push bc
          
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, b
      ld c, a ; c = a = (c * 8 + b)
      add a, a ; a = a * 2
      
      ld h, 0
      ld l, a
      add hl, de
      ld de, newColumnTiles
      ld b, 0
   -: 

      push bc

      push de
      push hl
      
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +   
      inc de
      sub 32
      ld c, a
+:
      ld hl, ItemblockBitmaskTbl
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      
      ;write out tile
      ldi
      ldi

      jp ++
+:
      ; write out empty tile
      inc hl
      inc hl
      xor a
      ld (de), a
      inc de
      ld (de), a
      inc de      
++:
      pop bc
   
      ld a, c
      add a, 8
      bit 6, a
      jp nz, + ;exit loop if c >= 64
      ld c, a
      
      push de
      ld d, 0
      ld e, 16 - 2
      add hl, de ; advance hl to point to next tile in the following row.
      pop de
      jp -
      
   +:
      ld h, d
      ld l, e
      
      jp ++++
;else
+++:
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      push bc
      ;a = (c * 8 + b) * 2
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, b
      add a, a
      
      ld h, 0
      ld l, a
      add hl, de
      ld de, newColumnTiles
      ld a, c
      neg
      add a, 8
      add a, a
   
      ld c, a ; c = (8 - c) * 2
      ld b, 0
      
   -: 
      ;write out tile
      ldi
      ldi
   
      jp po, + ;exit loop if bc = 0
      
      push de
      ld d, 0
      ld e, 16 - 2
      add hl, de ; advance hl to point to next tile in the following row.
      pop de
      jp -
      
   +:
      ld h, d
      ld l, e
;endif
++++:

.rept 2   
   pop bc
   pop de
   
   inc e
   
   push de
   push hl ; newTilePointer
   push bc
   call GetMapBlockNumber
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ld (curBlockNumber), de
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      
            
      ;a = b * 2
      ld a, b
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      pop de ; newTilesPointer
      
      push bc
      
      ld c, b
      ld b, 0
   -: 

      push bc

      push de
      push hl
      
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +   
      inc de
      sub 32
      ld c, a
+:
      ld hl, ItemblockBitmaskTbl
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      
      ;write out tile
      ldi
      ldi

      jp ++
+:
      ; write out empty tile
      inc hl
      inc hl
      xor a
      ld (de), a
      inc de
      ld (de), a
      inc de      
++:
      pop bc
   
      ld a, c
      add a, 8
      bit 6, a
      jp nz, + ;exit loop if c >= 64
      ld c, a
      
      push de
      ld d, 0
      ld e, 16 - 2
      add hl, de ; advance hl to point to next tile in the following row.
      pop de
      jp -
      
   +:
      ld h, d
      ld l, e
            
      jp ++++
;else
+++:
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      ;a = b * 2
      ld a, b
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      pop de ; newTilesPointer
      push bc
      ld bc, 8 * 2
   
   -: 
      ;write out tile  
      ldi
      ldi
   
      jp po, + ;exit loop if bc = 0
      
      push de
      ld d, 0
      ld e, 16 - 2
      add hl, de ; advance hl to point to next tile in the following row.
      pop de
      jp -
      
   +:
      ld h, d
      ld l, e
;endif
++++:
.endr

   pop bc
   pop de
   
   ld a, c
   or a
   jp z, writeOverscanTile   ; if y offset from last block was 0 (full block) then we don't need to process any further so just write 25th tile
   
   inc e
   
   ; write out the remaining tiles from the 4th block
   push de
   push hl ; newTilePointer
   push bc
   call GetMapBlockNumber
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ld (curBlockNumber), de
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      
            
      ;a = b * 2
      ld a, b
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      pop de ; newTilesPointer
      
      push bc
      
      ld a, b
      ld b, c ; b = number of tiles to write
      ld c, a ; c = index of tile in current block. 
   -: 

      push bc

      push de
      push hl
      
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +   
      inc de
      sub 32
      ld c, a
+:
      ld hl, ItemblockBitmaskTbl
      ld b, 0
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      
      ;write out tile
      ldi
      ldi

      jp ++
+:
      ; write out empty tile
      inc hl
      inc hl
      xor a
      ld (de), a
      inc de
      ld (de), a
      inc de      
++:
      pop bc
   
      ld a, c
      add a, 8
      ld c, a
      dec b
      jp m, + ;exit loop if b < 0
      
      push de
      ld d, 0
      ld e, 16 - 2
      add hl, de ; advance hl to point to next tile in the following row.
      pop de
      jp -
      
   +:
      ld h, d
      ld l, e
            
      jp ++++
;else
+++:
      GetBlockAddress
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      ;a = b * 2
      ld a, b
      add a, a
      ld h, 0
      ld l, a
      add hl, de
      pop de ; newTilesPointer
      push bc
      ld a, c
      add a, a
      ld c, a
      ld b, 0 ; bc = c * 2
   
   -: 
      ;write out tile  
      ldi
      ldi
   
      jp po, + ;exit loop if bc = 0
      
      push de
      ld d, 0
      ld e, 16 - 2
      add hl, de ; advance hl to point to next tile in the following row.
      pop de
      jp -
      
   +:

;endif
++++:
   
   pop bc
   pop de
   
writeOverscanTile:

   ; write the final 25th tile.
   
   ld a, (xScrollDir)
   or a
   jp p, + ; positive x direction so scrolling right.
   call GetMapBlockBottomLeft ; scroll left.
   jp ++
+:
   call GetMapBlockBottomRight
++:   
   push bc
   call GetMapBlockNumber
   
   ;new
   call IsItemBlock
   or a
   jp z, +++ ; not item block
;begin  -- IsItemBlock
      ld (curBlockNumber), de
      GetBlockAddress   
      ;de now contains the address of the block
      ;and the rom bank is loaded.
      pop bc
      ;b = x offset in block
      ;c = y offset in block
      
      
      ld a, c
      add a, a
      add a, a
      add a, a
      add a, b
      ld c, a ; c = c * 8 + b
      add a, a 
      ld h, 0
      ld l, a ;hl = (y * 8 + x) * 2
      add hl, de
      ld de, newColumnTiles + (24 * 2) ; location for the 25th tile

      ld b, 0
      push de
      push hl
      
      ld de, itemBlocks
      ;if c >= 32
      ld a, c 
      bit 5, a
      jp z, +   
      inc de
      sub 32
      ld c, a
+:
      ld hl, ItemblockBitmaskTbl
      add hl, bc
      ld a, (hl)
      ld hl, (curBlockNumber)
      add hl, hl ; x 2 because itemBlocks are words. 
      add hl, de
      and (hl)
      
      pop hl
      pop de
      jp nz, +
      
      ;write out tile
      ldi
      ldi

      jp ++
+:
      ; write out empty tile
      inc hl
      inc hl
      xor a
      ld (de), a
      inc de
      ld (de), a
      inc de      
++:      
      ret
;else
+++:
   
   ;old
   GetBlockAddress
   ;de now contains the address of the block
   ;and the rom bank is loaded.
   pop bc
   ;b = x offset in block
   ;c = y offset in block
   
   ;a = (c * 8 + b) * 2
   ld a, c
   add a, a
   add a, a
   add a, a
   add a, b
   add a, a
   ld h, 0
   ld l, a
   add hl, de
   ld de, newColumnTiles + (24 * 2) ; location for the 25th tile

   ;write out tile  
   ldi
   ldi
ret

; check to see if the current block is an item block
; de = block number.
; returns a = 1 if block is item block 0 otherwise.
IsItemBlock:
   ld a, d
   or a
   ld a, 0
   ret nz ; return if block > 128
   ld a, (curLevelInfo.numItemBlocks)
   cp e
   ld a, 0
   ret m ; return if block > curLevelInfo.numItemBlocks
   ret z ; return if block == curLevelInfo.numItemBlocks

   ld a, 1
ret

jp nz, +
xor a
ret

;E: columm number 
;returns DE = tilemap address for first tile in column
GetTileMapColumnAddress:
   push hl
   ld h, 0
   ld l, e
   add hl, hl ; hl = e * 2
   ld de, TILEMAP_START_ADDRESS
   add hl, de
   pop hl
ret
   

;BC: X starting coord (in tiles)
;DE: Y starting coord (in tiles)
WriteMapToTilemap:
   dec de
   dec de
   dec de
   
   ld hl, 0

   ; for(l=0;l<25;l++)
   ; for(h=0;h<32;h++)
   ;
   ld l, 25
-:
   push bc
   ld h, 0
--:   
   call WriteMapTile
   inc bc
   inc h
   ld a, h
   cp 32
   jp c, -- ; h < 32 repeat
   
   pop bc
   inc de
   inc l
   
   ld a, l
   cp 25
   jp z, +
   cp 28
   jp nz, -
   ld l, 0
   jp -
   
+:   
ret

; write map tile from (BC,DE) to tilemap (h,l)
;BC: tile X  coord
;DE: tile Y coord
; H: x pos on tilemap
; L: y pos on tilemap
;trashes a
WriteMapTile:
   push bc
   push de
   push hl
   push hl
   
   call GetMapBlockXYTiles
   push bc
   call GetMapBlockNumber
   GetBlockAddress
   ;de now contains the address of the block
   ;and the rom bank is loaded.
   pop bc
   ; b = x offset in block
   ; c = y offset in block
   
   ;a = (c * 8 + b) * 2
   ld a, c
   add a, a
   add a, a
   add a, a
   add a, b
   add a, a
   
   ld h, 0
   ld l, a
   add hl, de ; hl now contains a pointer to the tile in the block
   pop bc ; old hl the coord for the tile on the tilemap
   
   push hl
   
   ;hl = (c * 32 + b) * 2
   ld h, 0
   ld l, c
   .rept 5
   add hl, hl
   .endr
   ld c, b
   ld b, 0
   add hl, bc
   add hl, hl
   
   ld de, TILEMAP_START_ADDRESS
   add hl, de
   ld d, h
   ld e, l
   pop hl
   call TileMapSetTileFromHL
   

   pop hl
   pop de
   pop bc
ret

; returns block details for (mapX,MapY)
;
; Return data
; d = block x coord
; e = block y coord
;
; b = horizontal offset in the block
; c = vertical offset in the block

; trashed hl, a  
GetMapBlock:
   ld hl, (mapY)
   HLdiv8   ; hl = mapY / 8
   ld a, l
   and %111 ; a = (mapY / 8) % 8
   ld c, a
   HLdiv8   ; hl = mapY / 64
   ld e, l  ; A map cannot be more than 256 blocks high
            ; so the block number must be in l.
   
   ld hl, (mapX)
   HLdiv8   ; hl = mapX / 8
   ld a, l
   and %111 ; a = (mapX / 8) % 8
   ld b, a
   HLdiv8   ; hl = mapX / 64
   ld d, l  ; A map cannot be more than 256 blocks wide
            ; so the block number must be in l. 
ret

; returns block details for (X, Y) in pixels
; BC: X coord in pixels
; DE: Y coord in pixels
;
; Return data
; d = block x coord
; e = block y coord
;
; b = horizontal offset in the block
; c = vertical offset in the block

; trashed a  
GetMapBlockXYPixels:
   push hl
   ld h, d
   ld l, e
   HLdiv8   ; hl = mapY / 8
   ld a, l
   and %111 ; a = (mapY / 8) % 8
   ld d, a
   HLdiv8   ; hl = mapY / 64
   ld e, l  ; A map cannot be more than 256 blocks high
            ; so the block number must be in l.
            
   ld h, b
   ld l, c
   HLdiv8   ; hl = mapX / 8
   ld a, l
   and %111 ; a = (mapX / 8) % 8
   ld b, a
   ld c, d  ; y offset in block
   HLdiv8   ; hl = mapX / 64
   ld d, l  ; A map cannot be more than 256 blocks wide
            ; so the block number must be in l.
   pop hl
ret

; returns block details for (X, Y) in tiles
; BC: X coord in tiles
; DE: Y coord in tiles
;
; Return data
; d = block x coord
; e = block y coord
;
; b = horizontal offset in the block
; c = vertical offset in the block

; trashed a  
GetMapBlockXYTiles:
   push hl
   ld h, d
   ld l, e
   ld a, l
   and %111 ; a = (mapY / 8) % 8
   ld d, a
   HLdiv8   ; hl = mapY / 64
   ld e, l  ; A map cannot be more than 256 blocks high
            ; so the block number must be in l.
            
   ld h, b
   ld l, c
   ld a, l
   and %111 ; a = (mapX / 8) % 8
   ld b, a
   ld c, d  ; y offset in block
   HLdiv8   ; hl = mapX / 64
   ld d, l  ; A map cannot be more than 256 blocks wide
            ; so the block number must be in l.
   pop hl
ret

;same as above only it returns the block for then lower left corner or the screen.
;trashes hl, de
GetMapBlockBottomLeft:
   ld hl, (mapY)
   ld de, 24 * 8 ; bottom row of screen.
   add hl, de 
   HLdiv8   ; hl = mapY / 8
   ld a, l
   and %111 ; a = (mapY / 8) % 8
   ld c, a
   HLdiv8   ; hl = mapY / 64
   ld e, l  ; A map cannot be more than 256 blocks high
            ; so the block number must be in l.
   
   ld hl, (mapX)
   HLdiv8   ; hl = mapX / 8
   ld a, l
   and %111 ; a = (mapX / 8) % 8
   ld b, a
   HLdiv8   ; hl = mapX / 64
   ld d, l  ; A map cannot be more than 256 blocks wide
            ; so the block number must be in l. 
ret

;returns the block for then upper right corner or the screen.
;trashes hl, de
GetMapBlockTopRight:
   ld hl, (mapX)
   ld de, 31 * 8 ; right column of screen.
   add hl, de
   HLdiv8   ; hl = mapX / 8
   ld a, l
   and %111 ; a = (mapX / 8) % 8
   ld b, a
   HLdiv8   ; hl = mapX / 64
   ld d, l  ; A map cannot be more than 256 blocks wide
            ; so the block number must be in l. 
            
   ld hl, (mapY)
   HLdiv8   ; hl = mapY / 8
   ld a, l
   and %111 ; a = (mapY / 8) % 8
   ld c, a
   HLdiv8   ; hl = mapY / 64
   ld e, l  ; A map cannot be more than 256 blocks high
            ; so the block number must be in l.
   

ret

;returns the block for the lower right corner or the screen.
;trashes hl, de
GetMapBlockBottomRight:
   ld hl, (mapX)
   ld de, 31 * 8 ; right column of screen.
   add hl, de
   HLdiv8   ; hl = mapX / 8
   ld a, l
   and %111 ; a = (mapX / 8) % 8
   ld b, a
   HLdiv8   ; hl = mapX / 64
   ld d, l  ; A map cannot be more than 256 blocks wide
            ; so the block number must be in l. 
            
   ld hl, (mapY)
   push de
   ld de, 24 * 8 ; The line after the bottom row of screen.
   add hl, de 
   pop de
   HLdiv8   ; hl = mapY / 8
   ld a, l
   and %111 ; a = (mapY / 8) % 8
   ld c, a
   HLdiv8   ; hl = mapY / 64
   ld e, l  ; A map cannot be more than 256 blocks high
            ; so the block number must be in l.
   

ret

; d = block x coord
; e = block y coord
;
; returns block number in de
;
; trashes a, hl, bc
GetMapBlockNumber:
   ld a,  (curLevelInfo.blockMapBank)
   ld (BANK_SWITCH), a
   ld a, (curLevelInfo.mapWidthInBlocks)
   ld h, a
   call mul
   ;hl contains y * map_stride (in blocks))
   ld b, 0
   ld c, d
   add hl, bc ; hl = y * width + x 
   ld de, (curLevelInfo.blockMapAddress)
   add hl, hl

   add hl, de
   ld e, (hl)
   inc hl
   ld d, (hl)
   
   ;de contains block number.
ret
 
UpdateVdpScroll:
   ld a, (vdpXScrollReg)
    
   out (VDPStatusPort),a
   ld a,$88 ;FIXME define here
   out (VDPStatusPort),a
  
  
   ld a, (vdpYScrollReg)  
   out (VDPStatusPort),a
   ld a,$89 ;FIXME define here
   out (VDPStatusPort),a
   

   ld a, (drawNewColumn)
   dec a
   jp nz, +
   
   ; draw a new column of tiles.
   ld (drawNewColumn), a ; reset flag back to zero.
   ld h, a               ; h = 0
   ld a, (newTileXPos)
   ld l, a
   add hl, hl
   ld de, TILEMAP_START_ADDRESS ;FIXME need to handle map vertical offset.
   add hl, de
   
   ld a, (screenYPos)
   ld e, a
   DE_EQUALS_Ex64
   add hl, de
   
   ld de, newColumnTiles
   ld b, 50
-:   
   push hl ; tilemap pointer
   ld a,l
   out (VDPStatusPort), a
   ld a, h
   or $40
   out (VDPStatusPort), a
 
   ld h, d
   ld l, e
   ld c, VDPDataPort
   outi
   outi
   ld d, h
   ld e, l
   pop hl
   jp z, + ; finished.

   ;FIXME need to handle map vertical offset.
   ld a, b
   ld bc, 64
   add hl, bc
   ld b, a
   
   ;if hl > TILEMAP_START_ADDRESS + 28 * 64 then hl = hl - (28 * 64)
   ld a, h
   cp $3f
   jp c, ++
   ; greater or equal to TILEMAP_START_ADDRESS + 28 * 64 : $3f00
   push bc
   ld bc, -1792 ;-(28 * 64)
   add hl, bc
   pop bc
++:
   
   jp -
   
+: ; new row logic
   ld a, (drawNewRow)
   dec a
   ret nz
   
   ;draw new row of tiles
   ld (drawNewRow), a ; reset flag
   ld h, a            ; h = 0
   ld a, (newTileYPos)
   ld l, a            ; hl = (newTileYPos)
   HLx64
   
   ld de, TILEMAP_START_ADDRESS ;FIXME need to handle map horizontal offset.
   add hl, de
;old implementation 
;   ld a,l
;   out (VDPStatusPort), a
;   ld a, h
;   or $40
;   out (VDPStatusPort), a
;
;   ld hl, newRowTiles
;   ld c, VDPDataPort
;   .rept 64
;   outi
;   .endr


   push hl
   
;   ld a, (newTileXPos)
;   dec a
;   or a
;   jp p, +
;   ld a, 31
;+:

   ld a, (screenStartTileXPos)

;   inc a
;   cp 32
;   jp nz, +
;   ld a, 0
;+:

   add a, a
   ld d, 0
   ld e, a ; de = newTileXPos * 2

   add hl, de   ; hl = TILEMAP_START_ADDRESS + newTileYPos * 64
   
   add a, a
   ld e, a ; de = newTileXPos * 2 * 2
   ld ix, + ; jump pointer for our outi commands.
   add ix, de  
   
   ; write tilemap start address to vdp
   ld a,l
   out (VDPStatusPort), a
   ld a, h
   or $40
   out (VDPStatusPort), a
   
   ld hl, newRowTiles
   ld c, VDPDataPort
   
   jp (ix) ; perform 64 - (newTileXPos * 2) outi commands

+:
   .rept 64
   outi
   .endr
   
   ;write out the wrapped part of the new tile row.
   pop bc ; start of row address in tilemap. was in hl

   ld a, e 
   or d
   ret z   ; return if de = 0. all tiles where written by the previous outi instructions
         
   push hl
   ld hl, 64 * 2 ; total length of outi instructions
   xor a ; clear carry
   sbc hl, de
   ld d, h
   ld e, l
   ld ix, writeOuti64
   add ix, de ; ix = '+:' address +  128 - (newTileXPos * 2 * 2) 
   pop hl
   
   ; write tilemap start address to vdp
   ld a,c
   out (VDPStatusPort), a
   ld a, b
   or $40
   out (VDPStatusPort), a
   
   ld c, VDPDataPort
   
   jp (ix)

; do outi 64 times then return
writeOuti64:
   .rept 64
   outi
   .endr
   
   ret

PlayerUpdate:
   ld a, (player.currentFrame)
   push af
   
   ld a, (player.isDead)
   or a
   jp z, +
;begin  -- player.isDead
   ld a, LAD_SPRITE_ANGEL_OFFSET
   jp ++
;endif
+:
   
   ld a, (player.action)
   cp ACTION_STILL
   jp nz, +
   ; action still
   ld a, (player.dir)
   ld b, 1
   add a, b
   jp ++

+:
   cp ACTION_SKID   
   jp nz, +
   ; action skid
   ld a, (player.dir)
   ld b, LAD_SPRITE_SKID_OFFSET
   add a, b
   jp ++

+:
   cp ACTION_IN_THE_AIR   
   jp nz, +
   ; action in the air
   ld a, (player.wallSliding)
   or a
   ld b, 6 ; in the air
   jp z, +++
   ld b, 8 ; wall sliding
+++:
   ld a, (player.dir)
   add a, b
   jp ++

+:
   ; default walk/running
   ld a, (player.dir)
   ld b, a
   ld a, (player.x) ; load the low byte of the map.
   rra
   rra
   rra          ; a = a / 8
   and %11
   add a, LAD_SPRITE_WALK_OFFSET
   add a, b ; add direction sprite offset.

++:

   pop bc ; b = old frame
   cp b
   ret z ; return if no change in frame number.
   
   ;update frame here.
   ld (player.currentFrame), a ; update current frame
   ld a, 1
   ld (player.hasChangedFrame), a
ret

PlayerUpdateSprite:
   ld hl, (player.x)
   ld de, (mapX)
   xor a ; reset carry flag
   sbc hl, de
   ld a, l
   add a, 8 ; screen offset to account for hidden tile column
   ld b, a
   ld hl, (player.y)
   ld de, (mapY)
   xor a ; reset carry flag
   sbc hl, de ; player.y - mapY
   ld a, h
   or a
   jp p, +
      ; player.y < mapY
      ld a, l
      add a, 24 ; height of player sprite
      jp p, +
      ; player.y - mapY < -24
         ; cap the player sprite to make sure it doesn't appear on the bottom of the screen.
         ld l, -24
+:
   ld c, l
   dec c
   xor a
   ;a = 0 = tile number
   SpriteAdd ; SpriteAdd(0, x, y)
   
   ld a, b
   add a, 8
   ld b, a
   
   ld a, 2
   SpriteAdd ; SpriteAdd(2, x+8, y)
   
   ld a, c
   add a, 16
   ld c, a

   ld a, 6
   SpriteAdd ; SpriteAdd(6, x+8, y+16)
   
   ld a, b
   add a, -8
   ld b, a
   
   ld a, 4
   SpriteAdd ; SpriteAdd(4, x, y+16)
   
   ld a, (player.isDead)
   or a
   ret z
   
   ; player dead.
   ; show wing sprites
   ld a, b
   add a, -8
   ld b, a
   
   ld a, c
   add a, -8
   ld c, a

   ld a, LEFT_WING_SPRITE
   SpriteAdd ; SpriteAdd(4, x-8, y+8)
   
   ld a, b
   add a, 24
   ld b, a

   ld a, RIGHT_WING_SPRITE
   SpriteAdd ; SpriteAdd(4, x+16, y+*)
   
ret

PlayerMoveRight:
;      if player.vx >= 0 and player.vx <= INITIAL_WALK_VELOCITY then
;        player.vx = INITIAL_WALK_VELOCITY
;        player.x = player.x + 1
;      end
;
   ld a, (player.vx + 1)
   or a
   jp m, + ; player.xv < 0
   ld hl, (player.vx)
   ld bc, -(INITIAL_WALK_VELOCITY - 1)
   add hl, bc
   ld a, h
   or a
   jp p, + ; player.vx > INITIAL_WALK_VELOCITY
;begin  --  player.vx >= 0 and player.vx <= INITIAL_WALK_VELOCITY
   ld hl, INITIAL_WALK_VELOCITY
   ld (player.vx), hl
   ld hl, (player.x)
   inc hl
   ld (player.x), hl
;endif
+:

;      if player.vx < 0 then
;        player.vx = player.vx + SKID_DECELERATION
;        if player.action ~= ACTION_IN_THE_AIR then
;          player.action = ACTION_SKID
;        end
;      else
;        player.vx = player.vx + WALK_ACCELERATION
;        if player.action ~= ACTION_IN_THE_AIR then
;          player.action = ACTION_WALK
;        end
;      end
;

   ld a, (player.vx + 1) ; high byte
   or a
   jp p, ++
;begin  --  player.xv < 0
   ld hl, (player.vx)
   ld bc, SKID_DECELERATION
   add hl, bc
   ld (player.vx), hl
   
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp z, +++
   ;begin  --  player.action != ACTION_IN_THE_AIR
   ld a, ACTION_SKID
   ld (player.action), a
   ;endif
   +++:
   jp +
;else --  player.xv >= 0
++:
   ld hl, (player.vx)
   ld bc, WALK_ACCELERATION
   add hl, bc
   ld (player.vx), hl
   
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp z, +++
   ;begin  --  player.action != ACTION_IN_THE_AIR
   ld a, ACTION_WALK
   ld (player.action), a
   ;endif
   +++:
;endif  
+: 

;      if player.vx >= MAX_WALK_VELOCITY then
;        player.vx = MAX_WALK_VELOCITY
;      end
   ld hl, (player.vx)
   ld bc, -MAX_WALK_VELOCITY 
   add hl, bc
   ld a, h
   or a
   jp m, +
;begin  --  player.vx >= MAX_WALK_VELOCITY
   ld hl, MAX_WALK_VELOCITY
   ld (player.vx), hl
;endif
+:

   PlayerUpdateX
   PlayerSetDirectionRight    
ret

PlayerMoveLeft:
;      if player.vx <= 0 and player.vx >= -INITIAL_WALK_VELOCITY then
;        player.vx = -INITIAL_WALK_VELOCITY
;        player.x = player.x - 1
;      end
;
   ld hl, (player.vx)
   ld bc, INITIAL_WALK_VELOCITY
   add hl, bc
   ld a, h
   or a
   jp m, + ; player.vx < -INITIAL_WALK_VELOCITY
   ld hl, (player.vx)
   dec hl
   ld a, h
   or a
   jp p, + ; player.vx > 0
;begin  --  player.vx <= 0 and player.vx >= -INITIAL_WALK_VELOCITY
   ld hl, -INITIAL_WALK_VELOCITY
   ld (player.vx), hl
   ld hl, (player.x)
   dec hl
   ld (player.x), hl
;endif
+:

;      if player.vx > 0 then
;        player.vx = player.vx - SKID_DECELERATION
;        if player.action ~= ACTION_IN_THE_AIR then
;          player.action = ACTION_SKID
;        end
;      else
;        player.vx = player.vx - WALK_ACCELERATION
;        if player.action ~= ACTION_IN_THE_AIR then
;          player.action = ACTION_WALK
;        end
;      end
;

   ld hl, (player.vx)
   dec hl
   ld a, h
   or a
   jp m, ++
;begin  --  player.xv > 0
   ld hl, (player.vx)
   ld bc, -SKID_DECELERATION
   add hl, bc
   ld (player.vx), hl
   
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp z, +++
   ;begin  --  player.action != ACTION_IN_THE_AIR
   ld a, ACTION_SKID
   ld (player.action), a
   ;endif
   +++:
   jp +
;else --  player.xv <= 0
++:
   ld hl, (player.vx)
   ld bc, -WALK_ACCELERATION
   add hl, bc
   ld (player.vx), hl
   
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp z, +++
   ;begin  --  player.action != ACTION_IN_THE_AIR
   ld a, ACTION_WALK
   ld (player.action), a
   ;endif
   +++:
;endif  
+: 

;      if player.vx < -MAX_WALK_VELOCITY then
;        player.vx = -MAX_WALK_VELOCITY
;      end
   ld hl, (player.vx)
   ld bc, MAX_WALK_VELOCITY 
   add hl, bc
   ld a, h
   or a
   jp p, +
;begin  --  player.vx < -MAX_WALK_VELOCITY
   ld hl, -MAX_WALK_VELOCITY
   ld (player.vx), hl
;endif
+:

   PlayerUpdateX
   PlayerSetDirectionLeft
ret

PlayerNoDirectionPressed:
;   if player.vx > 0 then
;     player.vx = player.vx - WALK_DECELERATION
;     if player.vx < 0 then
;       player.vx = 0
;     end
;   elseif player.vx < 0 then
;     player.vx = player.vx + WALK_DECELERATION
;     if player.vx > 0 then
;       player.vx = 0
;     end
;   end

   ld hl, (player.vx)
   ld a, l
   or h
   jp z, ++
   ld a, h
   or a
   jp m, ++
;begin  --  player.vx > 0
   ld hl, (player.vx)
   ld bc, -WALK_DECELERATION
   add hl, bc
   ld (player.vx), hl
   
   ld a, h
   or a
   jp p, +++
   ;begin  --  player.vx < 0
      ld hl, 0
      ld (player.vx), hl
   ;endif
   +++:
   jp +
;else
++:
   ld hl, (player.vx)
   ld a, h
   or a
   jp p, +++
   ;begin  --  player.vx < 0
      ld bc, WALK_DECELERATION
      add hl, bc
      ld (player.vx), hl
      
      ld a, h
      or a
      jp m, ++++
      ld a, l
      or h
      jp z, ++++
      ;begin  --  player.vx > 0
         ld hl, 0
         ld (player.vx), hl
      ;endif
      ++++:
   ;endif
   +++:

;endif
+:

   PlayerUpdateX
;   
;   if player.vx == 0 and player.action ~= ACTION_IN_THE_AIR then
;     player.action = ACTION_STILL
;   end
   ld hl, (player.vx)
   ld a, h
   or l
   jp nz, +
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp z, +
;begin  --  player.vx == 0 && player.action != ACTION_IN_THE_AIR
   ld a, ACTION_STILL
   ld (player.action), a
;endif
+:

ret

PlayerJumpLogic:
;FIXME update player.wallSlide flag
   ld a, (buttonAPressed)
   or a
   jp z, ++
;begin  --  buttonAPressed
   call PlayerJumpButtonAPressed
   jp +
;else
++:
   ld hl, (player.vy)
   ld bc, GRAVITY_ACCELERATION
   add hl, bc
   ld (player.vy), hl   ; player.vy = player.vy + GRAVITY_ACCELERATION
   
   ld a, (player.wallSliding)
   or a
   jp z, ++
   ld hl, (player.vy)
   ld bc, -(MAX_WALL_SLIDE_VELOCITY + 1)
   add hl, bc
   ld a, h
   or a
   jp m, ++
   ;begin  --  player.wallSlide == 0 and player.vy > MAX_WALL_SLIDE_VELOCITY
      ld hl, MAX_WALL_SLIDE_VELOCITY
      ld (player.vy), hl
   ;endif
   ++:
;endif
+:

   ; player jump handle left/right buttons
   
   ld a, (lastInputState)
   ld b, a
   and P1_BUTTON_RIGHT
   jp nz,++ ; button not pressed so ignore.
   ;begin  --  right button pressed
   ld hl, (player.vx)
   ld a, h
   or a
   jp p, ++++
   ;begin  --  player.vx < 0
      ld bc, SKID_DECELERATION
      add hl, bc
      ld (player.vx), hl ; player.vx = player.vx + SKID_DECELERATION
      jp +++
   ;else
   ++++:
      ld bc, WALK_ACCELERATION
      add hl, bc
      ld (player.vx), hl ; player.vx = player.vx + WALK_ACCELERATION
   ;endif
   +++:
   ld hl, (player.vx)
   ld bc, -(MAX_WALK_VELOCITY+1)
   add hl, bc
   ld a, h
   or a
   jp m, +++
   ;begin  --  player.vx > MAX_WALK_VELOCITY
      ld hl, MAX_WALK_VELOCITY
      ld (player.vx), hl
   ;endif
   +++:
   PlayerSetDirectionRight
   jp +
++:
   ld a, b
   and P1_BUTTON_LEFT
   jp nz,+ ; button not pressed so ignore.
   ;begin  --  left button pressed
   ld hl, (player.vx)
   ld a, h
   or l
   jp z, ++++
   ld a, h
   or a
   jp m, ++++
   ;begin  --  player.vx > 0
      ld bc, -SKID_DECELERATION
      add hl, bc
      ld (player.vx), hl ; player.vx = player.vx - SKID_DECELERATION
      jp +++
   ;else
   ++++:
      ld bc, -WALK_ACCELERATION
      add hl, bc
      ld (player.vx), hl ; player.vx = player.vx - WALK_ACCELERATION
   ;endif
   +++:
   ld hl, (player.vx)
   ld bc, MAX_WALK_VELOCITY
   add hl, bc
   ld a, h
   or a
   jp p, +++
   ;begin  --  player.vx < -MAX_WALK_VELOCITY
      ld hl, -MAX_WALK_VELOCITY
      ld (player.vx), hl
   ;endif
   +++:
   PlayerSetDirectionLeft   
+: 

   PlayerUpdateX
   
   ld hl, (player.vy)
   ld bc, -(MAX_DOWNWARD_VELOCITY+1)
   add hl, bc
   ld a, h
   or a
   jp m, +
;begin  --  player.vy > MAX_DOWNWARD_VELOCITY
   ld hl, MAX_DOWNWARD_VELOCITY
   ld (player.vy), hl
;endif      
+:

ret

PlayerJumpButtonAPressed:
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp nz, +++
   ld a, (player.wallSliding)
   or a
   jp z, ++
;begin  --  player.action != ACTION_IN_THE_AIR || player.wallSlide != 0
+++:
   ld a, (buttonAHeld)
   or a
   jp nz, +
   ;begin  --  buttonAHeld = 0
      call PlayerStartJumping
   ;endif
   jp +
;else
++:
   ld hl, (player.vy)
   ld bc, $200
   add hl, bc
   ld a, h
   or a
   jp m, +++
   ;begin  --  player.vy >= -$200
      ld hl, (player.vy)
      ld bc, $50
      add hl, bc
      ld (player.vy), hl ; player.vy = player.vy + $50
      jp +
   ;else
   +++:
      ld hl, (player.vy)
      ld bc, $10
      add hl, bc
      ld (player.vy), hl ; player.vy = player.vy + $10
   ;endif
;endif
+:
ret

PlayerStartJumping:
   ld a, 1
   ld (buttonAHeld), a
   

   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp nz, ++
   ld a, (player.wallSliding)
   or a
   jp z, ++
;begin  --  player.wallSliding != 0 and player.action == ACTION_IN_THE_AIR
   cp WALL_SLIDE_RIGHT
   jp nz, ++++
   ;begin  --  player.wallSlide == WALL_SLIDE_RIGHT
      ld hl, -WALL_JUMP_X_VELOCITY
      ld (player.vx), hl
      PlayerSetDirectionLeft
      jp +++
   ;else
   ++++:
      ld hl, WALL_JUMP_X_VELOCITY
      ld (player.vx), hl
      PlayerSetDirectionRight
   ;endif
   +++:
   
   ld hl, -$370
   ld (player.vy), hl
   
   ;FIXME update camera level position here.
   jp +
;else
++:
   call PlayerJumpSetInitialVelocity
;endif
+:
   ;Removed jumpSfx - Niloct
   ;ld hl, JumpSfxData
   ;ld c, 1
   ;call PSGSFXPlay
   
   ld a, ACTION_IN_THE_AIR
   ld (player.action), a
ret

PlayerInitalJumpVelocityTbl:
.dw -$370
.dw -$390
.dw -$3b0
.dw -$3f0

;trashes a, bc, hl
PlayerJumpSetInitialVelocity:

;        if abs_xv < 0x0100 then
;          player.vy = -0x370
;        elseif abs_xv > 0x0300 then
;          player.vy = -0x3F0
;        elseif abs_xv > 0x0200 then
;          player.vy = -0x3B0
;        elseif abs_xv > 0x0100 then
;          player.vy = -0x390

   ld hl, (player.vx)
   call AbsHL
   ld a, h
   and %111
   add a, a
   ld hl, PlayerInitalJumpVelocityTbl
   ld b, 0
   ld c, a
   add hl, bc
   ld c, (hl)
   inc hl
   ld b, (hl)
   
   ld (player.vy), bc  
ret

;returns player position on the map measured in tiles
; bc = x pos
; de = y pos
;trashes hl
.macro PlayerGetMapPositionInTiles
   ld hl, (player.x)
   HLdiv8
   ld b, h
   ld c, l
   
   ld hl, (player.y)
   HLdiv8
   ld d, h
   ld e, l   
 .endm
      
;returns player screen position on the tilemap
; b = x pos
; c = y pos
;trashes a, de, hl
PlayerGetScreenPosition:
   ld hl, (player.x)
   ld a, l
   and %11111000
   ld l, a
   ld bc, (mapX)
   ld a, c
   and %11111000
   ld c, a
   xor a
   sbc hl, bc
   ld a, l
   sra a
   sra a
   sra a ; a /= 8
   ld b, a            ; b = ((player.x - (player.x % 8)) - (mapX - (mapX % 8))) / 8

   ld hl, (player.y)
   ld a, l
   and %11111000
   ld l, a
   ld de, (mapY)
   ld a, e
   and %11111000
   ld e, a
   xor a
   sbc hl, de
   ld a, l
   sra a
   sra a
   sra a ; a /= 8
   ld c, a            ; c = ((player.y - (player.y % 8)) - (mapY - (mapY % 8))) / 8
ret

PlayerCheckMapCollision:
;   ld a, (curLevelInfo.hitFlagsBank)
;   ld (BANK_SWITCH), a
;   call PlayerGetScreenPosition

   ld a, (player.vy + 1) ; high byte
   or a
   jp p, +
;begin  --  player.vy < 0
   ld hl, (player.oldX)   ; use old x position here to avoid restricting x movement when hitting the ceiling.
                          ; this also prevents the player sticking to walls when holding left or right next to a wall.
   HLdiv8
   ld b, h
   ld c, l
   
   ld hl, (player.y)
   HLdiv8
   ld d, h
   ld e, l  
   
   call PlayerCheckTopTiles
;endif 
+:

   PlayerGetMapPositionInTiles
   
   ld hl, (player.vx)
   ld a, h
   or a
   jp p, ++
;begin  --  player.vx < 0
   call PlayerCheckLeftTiles
   jp +
;else
++:
   or l
   jp z, +
   ; player.vx > 0 
   call PlayerCheckRightTiles
;endif 
+:

   PlayerGetMapPositionInTiles
   ;call PlayerGetScreenPosition

   ld a, (player.vy + 1) ; high byte
   or a
   jp m, +
;begin  --  player.vy >= 0
   call PlayerCheckBottomTiles
;endif 
+:
  
ret

;get the hitflags for a given tile
;hl tilenum
; returns hit flags in a
; trashes bc, d
.macro GetTileHitFlags
   ld a, (curLevelInfo.hitFlagsBank)
   ld (BANK_SWITCH), a
   ld bc, (curLevelInfo.hitFlagsAddress)
   ld a, h
   ld d, h
   and 1
   ld h, a ; clear off the tilemap flags
   add hl, bc
   ld a, (hl)
   bit TILE_BITFLAG_FORCE_PASSABLE, d
   jp z, +++
;begin  -- force passable
   and FORCE_PASSABLE_BITMASK
;endif
+++:
   bit TILE_BITFLAG_UPDATES_CAMERA_FLAG, d
   jp z, +++
;begin  --  update camera override flag.
   set BLOCK_UPDATES_CAMERA_FLAG, a
;endif
+++:
.endm

PlayerUpdateCamera:
;trashes a, bc, hl
   ld hl, (player.y)
   ld bc, -PLAYER_SCREEN_CENTRE_Y
   add hl, bc
   ld a, h
   or a
   jp p, +
   ;begin  --  player.y - PLAYER_SCREEN_CENTRE_Y < 0
      ld hl, 0
   ;endif
   +:
   ld (newMapY), hl
ret

;checks if the tile is damaging and if so updates the player death flag.
; A = tile hitflags
; trashes A
PlayerCheckForTileDamage:
   bit BLOCK_CAUSES_DAMAGE, a
   ret z
   call PlayerDead
ret

PlayerDead:
   ld a, 1
   ld (player.isDead), a
   ld a, ACTION_DEAD
   ld (player.action), a
   ld hl, 0
   ld (player.vx), hl
   ld hl, -ANGEL_RISE_VELOCITY
   ld (player.vy), hl
   call PlayerUpdate
ret

;bc = player pos x
;de = player pos y
PlayerCheckLeftTiles:
   push de
   push bc
   call PlayerHitTileAtXY ; player (x, y)
   pop bc
   pop de
   bit SOLID_RIGHT_FLAG, a
   jp nz, +
   
   inc de
   
   push de
   push bc
   call PlayerHitTileAtXY ; player (x, y + 1)
   pop bc
   pop de
   bit SOLID_RIGHT_FLAG, a
   jp nz, +
   
   inc de
   call PlayerHitTileAtXY ; player (x, y + 2)
   bit SOLID_RIGHT_FLAG, a
   jp z, ++
+:
;begin  --  tileIsSolidFromRight(tile) or tileIsSolidFromRight(tile1) or tileIsSolidFromRight(tile2)
   call PlayerCheckForTileDamage
   ld hl, 0
   ld (player.vx), hl    ; player.vx = 0
   
   ld hl, (player.x)
   ld a, l
   and %111
   add a, -8
   neg
   ld d, 0
   ld e, a
   add hl, de
   
   ld (player.x), hl     ; player.x = player.x + (8 - (player.x % 8))
;endif
++:
ret

PlayerCheckRightTiles:
   inc bc
   inc bc
   push de
   push bc
   call PlayerHitTileAtXY ; player (x + 2, y)

   pop bc
   pop de
   bit SOLID_LEFT_FLAG, a
   jp nz, +
   
   inc de
   
   push de
   push bc
   call PlayerHitTileAtXY ; player (x + 2, y + 1)

   pop bc
   pop de
   bit SOLID_LEFT_FLAG, a
   jp nz, +
   
   inc de
   
   call PlayerHitTileAtXY ; player (x + 2, y + 2)

   bit SOLID_LEFT_FLAG, a
   jp z, ++
+:
;begin  --  tileIsSolidFromLeft(tile) or tileIsSolidFromLeft(tile1) or tileIsSolidFromLeft(tile2)
   call PlayerCheckForTileDamage
   ld hl, 0
   ld (player.vx), hl    ; player.vx = 0
   
   ld a, (player.x) ; lsb
   and %11111000
   ld (player.x), a      ; player.x = player.x - player.x % 8
;endif
++:
ret

;bc = player map pos x
;de = player map pos y
PlayerCheckBottomTiles:
   inc de
   inc de
   inc de
   push de
   push bc
   call PlayerHitTileAtXY ; player (x, y + 3)

   pop bc
   pop de
   bit SOLID_TOP_FLAG, a
   jp nz, +
   inc bc
   push de
   push bc
   call PlayerHitTileAtXY ; player (x + 1, y + 3)

   pop bc
   pop de
   bit SOLID_TOP_FLAG, a
   jp nz, +
   ld a, (player.x)
   and %111           ; check to see if player.x is aligned to a tile boundary
   jp z, ++           ; player.x % 8 == 0
   
   inc bc             ; player.x not tile aligned so we need to check another tile.
   
   call PlayerHitTileAtXY  ; player (x + 2, y + 3)

   bit SOLID_TOP_FLAG, a
   jp z, ++
+:
;begin  --  tileIsSolid(tile) or tileIsSolid(tile1)
   ld (player.currentHitFlags), a
   call PlayerCheckForTileDamage
   ld a, (player.action)
   cp ACTION_IN_THE_AIR
   jp nz, +++
   ;begin  --  player.action == ACTION_IN_THE_AIR
      ld hl, 0
      ld (player.vy), hl    ; player.vy = 0
       
      ld a, ACTION_WALK
      ld (player.action), a ; player.action = ACTION_WALK
      
      ld a, (player.y) ; lsb
      and %11111000
      ld (player.y), a      ; player.y = player.y - player.y % 8
      ld a, (player.currentHitFlags)
      bit BLOCK_UPDATES_CAMERA_FLAG, a
      jp z, +++
      ;begin  --  if tile updates camera position
         call PlayerUpdateCamera
      ;endif
   ;endif
   +++:
   ret
;else
++:
   ld hl, (player.y)
   ld bc, -MAX_PLAYER_Y
   add hl, bc
   ld a, h
   or a
   jp m, ++++
   ;begin  --  player.y >= MAX_PLAYER_Y
      ld a, ACTION_WALK
      jp +++
   ;else
   ++++:
      ld a, ACTION_IN_THE_AIR
   ;endif
   +++:
   ld (player.action), a
;endif
ret

;b = player screen pos x
;c = player screen pos y
PlayerCheckTopTiles:
   ld a, (player.y+1)
   or a
   jp p, ++
;begin -- player.y < 0
      xor a
      ld (player.y), a
      ld (player.y+1), a ; clamp the player to the top of the map.
      jp +
;endif
++:
   push de
   push bc
   call PlayerHitTileAtXY ; player (x, y)

   pop bc
   pop de
   bit SOLID_BOTTOM_FLAG, a
   jp nz, +
   
   inc bc
   
   push de
   push bc
   call PlayerHitTileAtXY ; player (x + 1, y)

   pop bc
   pop de
   bit SOLID_BOTTOM_FLAG, a
   jp nz, +
   ld a, (player.x)
   and %111           ; check to see if player.x is aligned to a tile boundary
   jp z, ++           ; player.x % 8 == 0
   
   inc bc             ; player.x not tile aligned so we need to check another tile.
   
   call PlayerHitTileAtXY  ; player (x + 2, y)

   bit SOLID_BOTTOM_FLAG, a 
   jp z, ++
+:
;begin  --  tileIsSolidFromBottom(tile) or tileIsSolidFromBottom(tile1)
   call PlayerCheckForTileDamage
   ld hl, 0
   ld (player.vy), hl    ; player.vy = 0
   
   ld hl, (player.y)
   ld a, l
   and %111
   add a, -8
   neg
   ld d, 0
   ld e, a
   add hl, de
   
   ld (player.y), hl     ; player.y = player.y + (8 - (player.y % 8))
;endif
++:
ret

PlayerCheckHitAgainstObject:
   ld ix, (curObjectPtr)
   
   ld de, (player.y)
   ld l, (ix+object.y)
   ld h, (ix+object.y+1)
   ld bc, MONSTER_HEIGHT
   add hl, bc                 ; hl = enemy.y + enemy.height
   xor a
   sbc hl, de                 ; 
   jp m, +                    ; player.y > enemy.y + enemy.height
   
   ld hl, (player.y)
   ld bc, PLAYER_HEIGHT
   add hl, bc                 ; hl = player.y + player.height
   ld e, (ix+object.y)
   ld d, (ix+object.y+1)
   xor a
   sbc hl, de
   jp m, +                    ; player.y + player.height < enemy.y
   
   ld de, (player.x)
   ld l, (ix+object.x)
   ld h, (ix+object.x+1)
   
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_LONG, a
   jp z, ++
;begin  --  long object 3x2
   ld bc, MONSTER_BASE_WIDTH + 8
   jp +
;else
++:      
   ld bc, MONSTER_BASE_WIDTH
;endif
+:
 
   add hl, bc                 ; hl = enemy.x + enemy.width
   xor a
   sbc hl, de                 ; 
   jp m, +                    ; player.x > enemy.x + enemy.width
   
   ld hl, (player.x)
   ld bc, PLAYER_WIDTH
   add hl, bc                 ; hl = player.x + player.width
   ld e, (ix+object.x)
   ld d, (ix+object.x+1)
   xor a
   sbc hl, de
   jp m, +                    ; player.x + player.width < enemy.x
;begin  --  player has hit enemy
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_STATIC, a
   jp nz, PlayerHitStaticObject ; this will return.
   
   ;check for stomp attack.
   call PlayerCheckForStomp
   or a
   jp z, +++
   ;begin  --  stomp collision
      ld a, (ix+object.flags)
      res OBJ_BITFLAG_ACTIVE, a
      set OBJ_BITFLAG_DONT_RESPAWN, a
      ld (ix+object.flags), a ; object.active = false ; object.dont_respawn = true
      ld hl, -STOMP_ATTACK_VELOCITY
      ld (player.vy), hl                        ; player.vy = -$400
   jp ++
   ;else
   +++:
      call PlayerDead
   ;endif
   ++:
   ret
;endif
+:   
ret

;Handle player touching a static object.
; a = object flags
; trashes a, bc, hl
PlayerHitStaticObject:
   res OBJ_BITFLAG_ACTIVE, a
   set OBJ_BITFLAG_DONT_RESPAWN, a
   ld (ix+object.flags), a
   
   ld a, (ix+object.type)
   add a, a
   ld hl, ObjectUpdateFunctionTbl
   ld b, 0
   ld c, a
   add hl, bc
   ld c, (hl)
   inc hl
   ld h, (hl)
   ld l, c

   jp (hl) ; this function will return.


;check to see if we stomped on the object
;ix = curObjectPtr
PlayerCheckForStomp:
   ld hl, (player.y)
   ld d, (ix+object.y+1)
   ld e, (ix+object.y)
   xor a
   sbc hl, de
   jp p, +     ; player.y >= enemy.y
   
   ld hl, (player.y)
   ld bc, PLAYER_HEIGHT - 9
   add hl, bc                 ; hl = player.y + player.height
   ld e, (ix+object.y)
   ld d, (ix+object.y+1)
   xor a
   sbc hl, de                 ; player.y + player.height - enemy.y - 9
   jp p, +                    ; player.y + player.height - enemy.y > 8
   
   ld a, (player.vy+1)
   or a
   jp m, +                    ; player.vy < 0
;begin  -- is stomp collision
   ;Sfx stomp collision - Niloct
   ld hl, StompSfxData
   ld c, 1
   call PSGSFXPlay
   ;Sfx stomp collition - Niloct
   ld a, 1
   ret
;endif
+:
   xor a ; not a stomp collision
ret

PlayerCheckForEnemyCollision:
   ld hl, objects + object.flags
   ld a, (objectsTotal)
   ld b, a
-:
   ld a, (hl)
   and OBJ_FLAG_ACTIVE
   jp z, +
;begin  --  object[i] is active
   push hl
   push bc
   dec hl
   ld (curObjectPtr), hl
   call PlayerCheckHitAgainstObject
   pop bc
   pop hl
;endif
+:
   ld de, _sizeof_object
   add hl, de
   
   djnz -
ret


 
; copy the first frame of player tiles into sprite tile vram
PlayerInitSpriteTiles:
   ld a,   :MonkeyladTiles
   ld (BANK_SWITCH), a
   
   ld hl, MonkeyladTiles
   
   ld a,$00
   out (VDPStatusPort),a
   ld a,$20|$40
   out (VDPStatusPort),a
   
   ld c, VDPDataPort
   ; write out 8 tiles to VRAM
   call writeOuti64 ; outi x 64
   call writeOuti64 ; outi x 64
   call writeOuti64 ; outi x 64
   call writeOuti64 ; outi x 64
ret

; Update the sprite tiles for the player.
; d = new frame number
; trashes a, bc, hl
PlayerUpdateSpriteTiles:
   ld a,   :MonkeyladTiles
   ld (BANK_SWITCH), a
   
   ld hl, MonkeyladTiles
   ld a, h
   add a, d ; monkey sprites are 256 bytes so we can index like this. :)
   ld h, a
   
   ld a,$00
   out (VDPStatusPort),a
   ld a,$20|$40
   out (VDPStatusPort),a
   
   ld c, VDPDataPort
   ; write out 4 tiles to VRAM
   call writeOuti64 ; outi x 64
   call writeOuti64 ; outi x 64
   call writeOuti64 + 64 ; outi x 32
   
   ld bc, 32
   add hl, bc
   
   ld a,$c0
   out (VDPStatusPort),a
   ld a,$20|$40
   out (VDPStatusPort),a
   
   ld c, VDPDataPort
      
   call writeOuti64 + 64 ; outi x 32
      
ret

; b = x pos on screen
; c = y pos on screen
; tile returned in hl
;trashes a, bc, de, hl
GetTileFromTileMap:
   ld a, (screenYPos)
   add a, c
   cp 28
   jp c, +
   ;begin  -- screenXPos + c >= 28
   add a, -28
   ;endif 
+:
   ld e, a
   DE_EQUALS_Ex64
   
   ld a, (screenStartTileXPos)
;   ld c, a
;   ld a, (xScrollDir)
;   add a, c
   add a, b
   and %11111 ; a = (screenStartTileXPos + b) % 32
   add a, a   ; a *= 2
   ld h, 0
   ld l, a
   
   ld bc, TILEMAP_START_ADDRESS
   add hl, bc
   add hl, de ; hl = TILEMAP_START_ADDRESS + a + de
   
   TileMapReadTile ; hl now contains the tile number
ret

; Gets the tile hit flags for a given map coord
; Also handles item collection for item tiles.
; BC: X coord in tiles
; DE: Y coord in tiles
; returns tile hit flags in a
PlayerHitTileAtXY:
   push bc
   push de
   call GetMapBlockXYTiles
   push bc
   call GetMapBlockNumber
   push de ; block number
   GetBlockAddress
   pop hl ;block number
   pop bc
   push hl
   ;a = (c * 8 + b) * 2
   ld a, c
   add a, a
   add a, a
   add a, a
   add a, b
   add a, a
   ld h, 0
   ld l, a
   add hl, de
   ld e, (hL)
   inc hl
   ld h, (hl)
   ld l, e
   ;tile number in hl
   ld (curTileNumber), hl

   GetTileHitFlags
   pop hl ; block number

   pop de
   pop bc
   push af ; hitflags
   call PlayerCheckForItem
   pop af

ret


; BC: X coord in tiles
; DE: Y coord in tiles
; HL: block number

PlayerCheckForItem:
   ld a, h
   or a
   ret nz ; return if block > 128

   ld a, (curLevelInfo.numItemBlocks)
   cp l
   ret z ; return if block == curLevelInfo.numItemBlocks
   ret m ; return if block > curLevelInfo.numItemBlocks

   ; check that tilenumber <= num item tiles.
   ld a, (curTileNumber+1)
   bit 0, a
   ret nz   ; return if tilenumber > 128

   ld a, (curTileNumber)
   or a
   ret z ; return if tilenumber == 0

   ld ix, curLevelInfo.numItemTiles
   cp (ix+0)
   jp z, +
   ret nc   ; return if tilenumber > curLevelInfo.numItemTiles
+:
   push bc
   push hl
   call UpdateItemBlock
   pop bc
   pop hl ; x coord now in hl

   or a
   ret z ; return if a == 0 item already collected.
   
   ; FIXME remove item from map

   
   res 0, l ; x = x % 2
   
   ld bc, (mapX)
   BCdiv8
   
   or a ; reset carry
   sbc hl, bc ; hl = (x % 2) - mapX/8
   ld b, l
   
   ld h, d
   ld l, e
   res 0, l ; y = y % 2
   
   ld de, (mapY)
   DEdiv8
   
   or a ; reset carry
   sbc hl, de ; hl = (y % 2) - mapY/8
   ld c, l
   
   ; b = x coord on screen
   ; c = y coord on screen
   push bc
   call ClearTileFromTileMap
   pop bc
   
   inc c
   
   push bc
   call ClearTileFromTileMap   ; x, y + 1
   pop bc
   
   inc b
   
   push bc
   call ClearTileFromTileMap   ; x + 1, y + 1
   pop bc
   
   dec c
   jp ClearTileFromTileMap     ; x + 1, y
;ret

; Mark the item as collected in the item block.
; BC: X coord in tiles
; DE: Y coord in tiles
; HL: block number

; returns 1 in a if item was collected this call.
; 0 otherwise.
;Trashes af, bc, hl, ix
UpdateItemBlock:
   ld a, e
   and %110
   srl a
   add a, a
   add a, a
   
   ld b, a ; b = (y % 8 % 2) * 4
      
   ld a, c
   and %110
   srl a
   add a, b ; c = b + (x % 8 % 2) 
   
   add hl, hl
   ld bc, itemBlocks
   add hl, bc
   
   bit 3, a
   jp z, + ; idx < 8
   inc hl ; idx > 7 so move to next byte.
   and %111 ; idx = idx % 8
+:

   ld ix, bitMasks
   ld b, 0
   ld c, a
   add ix, bc
   ld a, (ix+0) ; load the bitmask into a
   
   ld b, a
   ld c, (hl)
   and c
   ld a, 0
   ret nz ; if bit is already set then nothing to do just return
   
   ld a, b

   or c
   ld (hl), a
   
   ld hl, StompSfxData ;FIXME need another SFX here.
   ld c, 1
   call PSGSFXPlay
   
   ld a, 1
ret

bitMasks:
.db %00000001
.db %00000010
.db %00000100
.db %00001000
.db %00010000
.db %00100000
.db %01000000
.db %10000000

LoadObjects:
   ld a, (curLevelInfo.objectDataBank)
   ld (BANK_SWITCH), a
   ld hl, (curLevelInfo.objectDataAddress)
   ld a, (hl) ; number of objects
   ld (objectsTotal), a
   ld c, a     ; c = totalnumber of objects
   ld de, objects
   ld hl, (curLevelInfo.objectDataAddress)
   inc hl
-:
   push bc
   ldi ; object number
   ldi ; object flags
   ldi ; x pos
   ldi 
   ldi ; y pos
   ldi
   ldi ; data
   ldi 
   ldi ; counter
   ldi 
   ldi ; current tile
   
   pop bc
   dec c
   ld a, c
   or c
   jp nz, -
   
ret

; activate objects that are near the player.
ObjectsActivate:
   ld hl, objects + object.flags
   ld a, (objectsTotal)
   ld c, a
-:
   ld a, (hl)
   and OBJ_FLAG_DONT_RESPAWN
   jp nz, +
;begin  --  object[i] can be activated
   push hl
   push bc
   dec hl
   ld (curObjectPtr), hl
   call ObjectUpdateActiveState
   pop bc
   pop hl
;endif
+:
   ld de, _sizeof_object
   add hl, de
   
   dec c
   jp nz, -

ret

ObjectUpdateActiveState:
   ld ix, (curObjectPtr)

   ld hl, (mapX)
   ld bc, -64
   add hl, bc
   ld b, (ix+object.x+1)
   ld c, (ix+object.x)
   xor a
   sbc hl, bc
   jp p, ++ ; object.x < mapX - 64
   
   ld hl, (mapX)
   ld de, SCREEN_WIDTH + 64
   add hl, de
   xor a
   sbc hl, bc
   jp m, ++ ; object.x > mapX + SCREEN_WIDTH + 64
;begin  --  object.x >= mapX - 64 && object.x <= mapX + SCREEN_WIDTH + 64
   set OBJ_BITFLAG_ACTIVE, (ix+object.flags) ; activate object
   jp +
;else
++:   
   res OBJ_BITFLAG_ACTIVE, (ix+object.flags) ; deactivate object
;endif
+:

ret

ObjectUpdateFunctionTbl:
.dw ObjectUpdateNone
.dw ObjectUpdateSnail
.dw ObjectUpdateBird
.dw ObjectUpdateFish
.dw ObjectUpdateLevelEnd
.dw ObjectUpdateNewLife

ObjectsUpdate:
   ld hl, objects + object.flags
   ld a, (objectsTotal)
   ld c, a
-:
   ld a, (hl)
   bit OBJ_BITFLAG_ACTIVE, a
   jp z, +
   bit OBJ_BITFLAG_STATIC, a
   jp nz, +
;begin  --  object[i] is active and not static
   push hl
   push bc
   dec hl
   ld (curObjectPtr), hl
   ld a, (hl) ; object type
   add a, a
   ld hl, ObjectUpdateFunctionTbl
   ld b, 0
   ld c, a
   add hl, bc
   ld c, (hl)
   inc hl
   ld h, (hl)
   ld l, c
   ld bc, ++
   push bc
   jp (hl)
++:
   pop bc
   pop hl
;endif
+:
   ld de, _sizeof_object
   add hl, de
   
   dec c
   jp nz, -

ret

; object update routines
ObjectUpdateNone:
ret

ObjectUpdateSnail:
   ld ix, (curObjectPtr)
   
   ld c, (ix+object.x)
   ld b, (ix+object.x+1)
   
   ld d, (ix+object.counter+1)
   ld e, (ix+object.counter)
   dec de
   ld a, d
   or e
   jp nz, +
;begin  --  counter == 0
   ld d, (ix+object.data+1) ; counter = data
   ld e, (ix+object.data)
   
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_DIR_LEFT, a
   jp z, +++
   ;begin  --  LEFT
      and OBJ_FLAG_DIR_CLEAR_MASK
      or OBJ_FLAG_DIR_RIGHT
   jp ++
   ;else
   +++:
      and OBJ_FLAG_DIR_CLEAR_MASK
      or OBJ_FLAG_DIR_LEFT
   ;endif
   ++:
   ld (ix+object.flags), a
;endif   
+:
   ld (ix+object.counter+1), d
   ld (ix+object.counter), e
   ld a, e
   and 3
   cp 3
   jp nz, +
;begin  --  counter % 3 == 0
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_DIR_LEFT, a
   jp z, +++
   ;begin  --  direction LEFT
      ld a, $4c      
      dec bc
   jp ++
   ;else
   +++:
      ld a, $44 
      inc bc
   ;endif
   ++:
   ld (ix+object.x), c
   ld (ix+object.x+1), b
   
   ; update frame a contains base tile for current direction
   bit 2, e
   jp z, ++
   ;begin  --  bit 3 set
      add a, 4
   ;endif
   ++:
   ld (ix+object.curTile), a
;endif
+:

ret

ObjectUpdateBird:
;FIXME need to DRY up this code. could probably be shared with snail
   ld ix, (curObjectPtr)
   
   ld c, (ix+object.x)
   ld b, (ix+object.x+1)
   
   ld d, (ix+object.counter+1)
   ld e, (ix+object.counter)
   dec de
   ld a, d
   or e
   jp nz, +
;begin  --  counter == 0
   ld d, (ix+object.data+1) ; counter = data
   ld e, (ix+object.data)
   
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_DIR_LEFT, a
   jp z, +++
   ;begin  --  LEFT
      and OBJ_FLAG_DIR_CLEAR_MASK
      or OBJ_FLAG_DIR_RIGHT
   jp ++
   ;else
   +++:
      and OBJ_FLAG_DIR_CLEAR_MASK
      or OBJ_FLAG_DIR_LEFT
   ;endif
   ++:
   ld (ix+object.flags), a
;endif   
+:
   ld (ix+object.counter+1), d
   ld (ix+object.counter), e
   ld a, e
   and %11
   cp 3
   jp nz, +
;begin  --  counter % 3 == 0
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_DIR_LEFT, a
   jp z, +++
   ;begin  --  direction LEFT
      ld a, $60
      dec bc
      dec bc
   jp ++
   ;else
   +++:
      ld a, $54
      inc bc
      inc bc
   ;endif
   ++:
   ld (ix+object.x), c
   ld (ix+object.x+1), b
   
   ; update frame a contains base tile for current direction
   bit 3, e
   jp z, ++
   ;begin  --  bit 4 set
      add a, 6
   ;endif
   ++:
   ld (ix+object.curTile), a
;endif
+:

ret

FishTbl:
.include "fish_tbl.inc"

FishWaitTbl:
.dw 0
.dw 30
.dw 60
.dw 120

ObjectUpdateFish:
   ld ix, (curObjectPtr)
   
   ld a, (ix+object.counter+1)
   bit 7, a
   jp nz, +
   ;begin -- bit 7 not set in counter high byte. We just dec the counter and continue
   ld b, a
   ld c, (ix+object.counter)
   or c
   jp nz, ++
   ; begin counter == 0
      set 7, b
      ld (ix+object.counter+1), b
      ret
   ;endif
   ++:
   
   dec bc
   ld (ix+object.counter), c
   ld (ix+object.counter+1), b
   ret
   
+:   
   ld c, (ix+object.y)
   ld b, (ix+object.y+1)
   
   ld e, (ix+object.counter)
   ld a, e
   cp 70
   jp nz, +
;begin  --  counter == 70
;   ld d, (ix+object.data+1) ; counter = data
;   ld e, (ix+object.data)
   ld e, 0
   ; fixme need to set wait counter initial value here.
   ld c, (ix+object.data)
   ld a, (ix+object.data+1)
   and %00111111
   ld (ix+object.y), c
   ld (ix+object.y+1), a ;reset y to initial value stored in object.data
   
   ld a, (ix+object.flags)
   and OBJ_FLAG_DIR_CLEAR_MASK
   or OBJ_FLAG_DIR_RIGHT
   ld (ix+object.flags), a
   
   ld a, (ix+object.data+1)
   and %11000000
   rlc a
   rlc a
   add a, a
   ld hl, FishWaitTbl
   ld c, a
   ld b, 0
   add hl, bc
   ld c, (hl)
   inc hl
   ld b, (hl)
   ld (ix+object.counter), c    ; extract the initial wait time from 2 high bits in object.data
   ld (ix+object.counter+1), b  ; use value to lookup wait time in FishWaitTbl
   ret
   jp ++
;elseif counter == 35 
+:
   cp 35
   jp nz, ++
      ld a, (ix+object.flags)
      and OBJ_FLAG_DIR_CLEAR_MASK
      or OBJ_FLAG_DIR_LEFT
      ld (ix+object.flags), a
;endif
++:
   
   ld hl, FishTbl
   add hl, de
   ld a, (hl)
   ld l, a
   SignExtend_HL
   add hl, bc
   ld (ix+object.y), l
   ld (ix+object.y+1), h
   inc e
   ld (ix+object.counter), e
   ld a, e
   and %11
   cp 3
   jp nz, +
;begin  --  counter % 3 == 0
   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_DIR_LEFT, a
   jp z, +++
   ;begin  --  direction LEFT
      ld a, $3c
   jp ++
   ;else
   +++:
      ld a, $34
   ;endif
   ++:
   
   ; update frame a contains base tile for current direction
   bit 2, e
   jp z, ++
   ;begin  --  bit 4 set
      add a, 4
   ;endif
   ++:
   ld (ix+object.curTile), a
;endif
+:

ret

ObjectUpdateLevelEnd:
   ld ix, (curObjectPtr)
   ld hl, gameFlags
   set BITFLAG_NEXT_LEVEL, (hl)
   
   ld a, (ix+object.data)
   ld (nextLevel), a

ret

ObjectUpdateNewLife:
   xor a ; clear carry flag
   ld a, (player.lifeCounter)
   inc a
   daa
   ld (player.lifeCounter), a
   call HUDUpdate
   ;FIXME need new SFX when getting a new life
   ld hl, StompSfxData
   ld c, 1
   call PSGSFXPlay
ret

ObjectsUpdateSprites:
   ld hl, objects + object.flags
   ld a, (objectsTotal)
   ld c, a
-:
   ld a, (hl)
   and OBJ_FLAG_ACTIVE
   jp z, +
;begin  --  object[i] is active
   push hl
   push bc
   dec hl
   ld (curObjectPtr), hl
   call ObjectAddSprites
   pop bc
   pop hl
;endif
+:
   ld de, _sizeof_object
   add hl, de
   
   dec c
   jp nz, -

ret

; add sprites for object

;get screen pos for object
;hl start of object position data
; returns
; bc : x pos
; de : y pos
; trashes hl, a
.macro ObjGetScreenPos
   ld c, (hl)     ; load obj.x into bc
   inc hl
   ld b, (hl)
   inc hl
   push hl
   ld h, b
   ld l, c
   ld bc, (mapX)
   xor a
   sbc hl, bc
   ld b, h
   ld c, l        ; bc = obj.x - mapX
   pop hl
   
   ld e, (hl) 
   inc hl
   ld h, (hl)
   ld l, e        ; load obj.y into hl
   ld de, (mapY)
   xor a
   sbc hl, de
   ld d, h
   ld e, l        ; de = obj.y - mapY
.endm

ObjectAddSprites:
   ld a, (hl) ; type
   inc hl
   inc hl     ; flags
   
   ObjGetScreenPos
   ; bc = sx
   ; de = sy
   
   ld a, d
   or a
   jp p, +
   ld a, e
   add a, 8
   jp p, +
;begin  --  sy < -8
   ret
;endif
+:

   ld hl, -24 * 8
   add hl, de
   ld a, h
   or a
   jp m, +
;begin  --  sy > 24 * 8
   ret
+:

   dec de ; sy -= 1 to align with bg tiles.
 
   ld hl, 8
   add hl, bc
   ld b, h
   ld c, l        ; sx += 8
   ld a, h
   or a
   jp m, +
   
   ld hl, -32 * 8
   add hl, bc
   ld a, h
   or a
   jp p, +
;begin  --  sx >= 0 && sx < 32 * 8
   push bc
   push de
   ld b, c
   ld c, e
   ld ix, (curObjectPtr)
   ld a, (ix+object.curTile)
   SpriteAdd
   pop de
   pop bc
;endif
+:

   ld hl, 8
   add hl, bc
   ld b, h
   ld c, l        ; sx += 8
   ld a, h
   or a
   jp m, +
   
   ld hl, -32 * 8
   add hl, bc
   ld a, h
   or a
   jp p, +
;begin  --  sx >= 0 && sx < 32 * 8
   push bc
   push de
   ld b, c
   ld c, e
   ld ix, (curObjectPtr)
   ld a, (ix+object.curTile)
   add a, 2
   SpriteAdd
   pop de
   pop bc
;endif
+:

   ld a, (ix+object.flags)
   bit OBJ_BITFLAG_LONG, a
   ret z ; return if not a long object
    
   ld hl, 8
   add hl, bc
   ld b, h
   ld c, l        ; sx += 8
   ld a, h
   or a
   jp m, +
   
   ld hl, -32 * 8
   add hl, bc
   ld a, h
   or a
   jp p, +
;begin  --  obj.flags == long object && sx >= 0 && sx < 32 * 8
   ld b, c
   ld c, e
   ld ix, (curObjectPtr)
   ld a, (ix+object.curTile)
   add a, 4
   SpriteAdd
;endif
+:

ret

CameraUpdate:

   ld hl, (mapX)
   ld b, h
   ld c, l

   ld de, -(PLAYER_SCREEN_CENTRE_X)
   ld hl, (player.x)
   add hl, de
   
   ld a, h
   or a
   jp p, ++
; begin  --  if player.x - PLAYER_SCREEN_CENTRE_X < 0
   ld hl, 0
   jp +
;else
++:
   ld de, (maxMapX)
   xor a
   sbc hl, de
   jp m, ++++
   ;begin  --   if player.x - PLAYER_SCREEN_CENTRE_X >= maxMapX
      ld h, d
      ld l, e ; maxMapX
      jp +++
   ;else
   ++++:
      add hl, de ; restore back to :: player.x - PLAYER_SCREEN_CENTRE_X
   ;endif
   +++:     
;endif   
+:

   xor a
   sbc hl, bc
   
   
   push hl
   
;   ld hl, (mapY)
;   ld b, h
;   ld c, l
;
;   ld de, -PLAYER_SCREEN_CENTRE_Y
;   ld hl, (player.y)
;   add hl, de
;
;   ld a, h
;   or a
;   jp p, ++
;; begin  --  if player.y - PLAYER_SCREEN_CENTRE_Y < 0
;   ld hl, 0
;   jp +
;;else
;++:
;   ld de, (maxMapY)
;   xor a
;   sbc hl, de
;   jp m, ++++
;   ;begin  --   if player.y - PLAYER_SCREEN_CENTRE_Y >= maxMapY
;      ld h, d
;      ld l, e ; maxMapY
;      jp +++
;   ;else
;   ++++:
;      add hl, de ; restore back to :: player.y - PLAYER_SCREEN_CENTRE_Y
;   ;endif
;   +++:   
;;endif   
;+:
;
;   xor a
;   sbc hl, bc
;   
;   ld c, l

   ; player.y - newMapY > PLAYER_SCREEN_Y_FALL_THRESHOLD
   ld hl, (player.y)
   ld de, -PLAYER_SCREEN_Y_FALL_THRESHOLD
   add hl, de
   ld de, (newMapY)
   xor a
   sbc hl, de
   ld a, h
   or a
   jp m, +
;begin  --  player.y - newMapY > PLAYER_SCREEN_Y_FALL_THRESHOLD
   add hl, de ; reset hl to player.y - PLAYER_SCREEN_Y_FALL_THRESHOLD by adding newMapY
   ld de, (maxMapY)
   xor a
   sbc hl, de
   jp m, ++++
   ;begin  --   if player.y - PLAYER_SCREEN_Y_FALL_THRESHOLD >= maxMapY
      ld h, d
      ld l, e ; maxMapY
      jp +++
   ;else
   ++++:
      add hl, de ; restore back to :: player.y - PLAYER_SCREEN_Y_FALL_THRESHOLD
   ;endif
   +++: 
   ld (newMapY), hl
;endif
+:

   ld hl, (mapY)
   ld de, (newMapY)
   xor a
   sbc hl, de
   ld a, h
   or a
   jp p, ++
;begin  -- mapY < newMapY
   ld a, 4
   add a, l ; l < 0
   or a
   jp m, +++
   ;begin  --   (mapY - newMapY) >= -4
      ld a, l
      neg
      ld c, a
   jp ++++
   ;else
   +++:
      ld c, 4
   ;endif
   ++++:
   jp +
;else
++:
   or l
   jp z, ++++
   ;begin  --  mapY > newMapY
      ld a, -4
      add a, l
      or a
      jp p, +++++ 
      ;begin  --  (mapY > newMapY) < 4
         ld a, l
         neg
         ld c, a ; c = -(mapY - newMapY)
      jp ++++++
      ;else
      +++++:
         ld c, -4
      ;endif
      ++++++:
      jp +++
   ;else  --  mapY == newMapY
   ++++:
      ld c, 0
   ;endif
   +++:
;endif
+:   
   pop hl

   ld b, l
   
   ld a, b
   or c
   ret z ; return if bc = 0. eg no scrolling
   
   jp ScrollMap ; ret

ShowGameOverScreen:
   ld a,   :GameOverTileData
   ld (BANK_SWITCH), a
   
   ld hl,GameOverPal
   ld b,(GameOverPalEnd-GameOverPal)
   call LoadPalette


   ld hl,GameOverTileData              ; Location of tile data
   ld bc,GameOverTileDataEnd-GameOverTileData  ; Counter for number of bytes to write
   call LoadTiles

   ld hl,GameOverTilemap
   ld bc,GameOverTilemapEnd-GameOverTilemap; Counter for number of bytes to write
   call LoadTilemap

   call ScreenOn

   ei

; show game over screen loop
-:
   call WaitVblank

   call PSGFrame

   ; any keypress on controller 1 or pause will exit the loop.
   in a, ($dc)
   xor $ff
   ld b, a
   ld a,(pauseFlag)
   or b
   jp z,-
   jp main ; restart the game.

ExitFromLevel:
   call PSGStop
   call PSGSFXStop
   call ScreenOff
   call SetupVDP
   xor a ; clear carry flag
   ld a, (player.lifeCounter)
   dec a
   daa
   or a
   jp z, ShowGameOverScreen
   
      ; clear player struct
   ld b, _sizeof_player_struct
   ld hl, player
   call ClearRAM
   
   ld (player.lifeCounter), a ; maintain player life counter.
   
   jp resetLevel

ReadInput:
   push af
   push bc
   in a, ($dc)
   ld b, a
   and P1_BUTTON_RIGHT 
   jp nz,+
   ld a, (lastInputState)
   and P1_BUTTON_RIGHT ; right button
   jp z,+ ; button still pressed so ignore.
+:
   ld a, b
   and P1_BUTTON_LEFT 
   jp nz,+
   ld a, (lastInputState)
   and P1_BUTTON_LEFT ; left button
   jp z,+ ; button still pressed so ignore.
+:
   ld a, b
   and P1_BUTTON_DOWN 
   jp nz,+
   ld a, (lastInputState)
   and P1_BUTTON_DOWN ; down button
   jp z,+ ; button still pressed so ignore.
+:
   ld a, b
   and P1_BUTTON_UP 
   jp nz,+
   ld a, (lastInputState)
   and P1_BUTTON_UP ; up button
   jp z,+ ; button still pressed so ignore.
+:
   ld a, b
   and P1_BUTTON_A 
   jp nz,+
   ld a, (lastInputState)
   and P1_BUTTON_A ; a button
   jp z,+ ; button still pressed so ignore.
+:
   ld a, b
   and P1_BUTTON_B 
   jp nz,+
   ld a, (lastInputState)
   and P1_BUTTON_B ; b button
   jp z,+ ; button still pressed so ignore.

+:
   ld a, b
   ld (lastInputState), a
   pop bc
   pop af
   ret

  

; zeros out B bytes of ram starting at HL
; B = number of bytes to clear
; HL = start address
; trashes b, hl 
ClearRAM:
-:
   ld (hl), 0
   inc hl
   djnz -
ret

InitRand:
   push bc
   ld bc, (randSeed)
   ld (randValue), bc
   pop bc
   ret
/*
RandValue:
; bc = randValue = randValue ^ $beef 
   push hl
   push af
   ld hl, (randValue)
   ld a, $be
   xor h
   rra
   ld b, a
   ld a, $ef
   xor l
   rla
   ld c, a
   inc bc
   ld (randValue), bc
   pop af
   pop hl
   ret
*/
GetRandomNumber:
; Uses a 16-bit RAM variable called RandomNumberGeneratorWord
; Returns an 8-bit pseudo-random number in a
    push hl
        ld hl,(randValue)
        ld a,h         ; get high byte
        rrca           ; rotate right by 2
        rrca
        xor h          ; xor with original
        rrca           ; rotate right by 1
        xor l          ; xor with low byte
        rrca           ; rotate right by 4
        rrca
        rrca
        rrca
        xor l          ; xor again
        rra            ; rotate right by 1 through carry
        adc hl,hl      ; add RandomNumberGeneratorWord to itself
        jr nz,+
        ld hl,$733c    ; if last xor resulted in zero then re-seed random number generator
+:      ld a,r         ; r = refresh register = semi-random number
        xor l          ; xor with l which is fairly random
        ld (randValue),hl
    pop hl
    ret                ; return random number in a


;Setup initial VDP register values
SetupVDP:
   ld hl, VdpInitData
   ld c, VDPStatusPort
   ld b, VdpInitDataEnd-VdpInitData
   otir

   call ClearVRAM
ret

.ends


.BANK 0 SLOT 0
.section "data" force 

ItemblockBitmaskTbl:
.db %00000001
.db %00000001
.db %00000010
.db %00000010
.db %00000100
.db %00000100
.db %00001000
.db %00001000
.db %00000001
.db %00000001
.db %00000010
.db %00000010
.db %00000100
.db %00000100
.db %00001000
.db %00001000
.db %00010000
.db %00010000
.db %00100000
.db %00100000
.db %01000000
.db %01000000
.db %10000000
.db %10000000
.db %00010000
.db %00010000
.db %00100000
.db %00100000
.db %01000000
.db %01000000
.db %10000000
.db %10000000

VdpInitData:
.db $04,$80
.db $84,$81
.db $ff,$82
.db $ff,$85
.db %11111111,$86 ; sprites come from the second 256 tiles.
.db $00,$87 ; overscan/background colour. (from sprite palette)
.db $00,$88
.db $00,$89
.db $ff,$8a
VdpInitDataEnd:

TitlePal:
.include "gfx/title_pal.inc"
TitlePalEnd:

LevelData:
;L 1-1
.include "maps/level1_1/level1_1_config.inc"
.db :Map_1_1_Palette ; bank for gfx assets
.dw Map_1_1_Palette
.db Map_1_1_Palette_End - Map_1_1_Palette
.dw Map_1_1_Tiles 
.dw Map_1_1_Tiles_End - Map_1_1_Tiles
.db :Map_1_n_MonsterTiles 
.dw Map_1_n_MonsterTiles 
.dw Map_1_n_MonsterTiles_End - Map_1_n_MonsterTiles
.db :Map_1_1_BlockMapData
.dw Map_1_1_BlockMapData
.db :Map_1_1_Blocks
.dw 0 ;10 ;71 ; starting coord X block
.dw 8 ;3  ; starting coord Y block
.dw PLAYER_SCREEN_CENTRE_X ; player starting screen offset x
.dw PLAYER_SCREEN_CENTRE_Y ; player starting screen offset y
.db :Music_1_n
.dw Music_1_n
.db :Map_1_1_HitFlags
.dw Map_1_1_HitFlags
.db :Map_1_1_Objects
.dw Map_1_1_Objects

;L 1-2
.include "maps/level1_2/level1_2_config.inc"
.db :Map_1_2_Palette ; bank for gfx assets
.dw Map_1_2_Palette
.db Map_1_2_Palette_End - Map_1_2_Palette
.dw Map_1_2_Tiles 
.dw Map_1_2_Tiles_End - Map_1_2_Tiles
.db :Map_1_n_MonsterTiles 
.dw Map_1_n_MonsterTiles 
.dw Map_1_n_MonsterTiles_End - Map_1_n_MonsterTiles
.db :Map_1_2_BlockMapData
.dw Map_1_2_BlockMapData
.db :Map_1_2_Blocks
.dw 75 ;10 ;71 ; starting coord X block
.dw 32 ;3  ; starting coord Y block
.dw PLAYER_SCREEN_CENTRE_X ; player starting screen offset x
.dw PLAYER_SCREEN_CENTRE_Y ; player starting screen offset y
.db :Music_1_n
.dw Music_1_n
.db :Map_1_2_HitFlags
.dw Map_1_2_HitFlags
.db :Map_1_2_Objects
.dw Map_1_2_Objects

.ends

.BANK 1 SLOT 1
.ORG $0000

TitleMusic:
.incbin "music/title.psg"

SfxData:

JumpSfxData:
.incbin "sfx/jump.psg"
JumpSfxDataEnd:

StompSfxData:
.incbin "sfx/stomp.psg"
StompSfxDataEnd:

.include "psglib/PSGlib.inc"


.BANK 2 SLOT 2
.ORG $0000
TitleTileData:
.include "gfx/title_tiles.inc"
TitleTileDataEnd:

TitleTilemap:
.include "gfx/title_tilemap.inc"
TitleTilemapEnd:

HUDTileData:
.include "gfx/HUD_tiles.inc"
HUDTileDataEnd:

MonkeyWingsTileData:
.include "gfx/monkeylad_angel_wings_tiles.inc"
MonkeyWingsTileDataEnd:

GameOverTileData:
.include "gfx/game_over_tiles.inc"
GameOverTileDataEnd:

GameOverTilemap:
.include "gfx/game_over_tilemap.inc"
GameOverTilemapEnd:

GameOverPal:
.include "gfx/game_over_pal.inc"
GameOverPalEnd:

.BANK 3 SLOT 2
.ORG $0000
Map_1_1_Palette:
.include "maps/level1_1/level1_1_pal.inc"
.db $39 ; overwrite the first sprite palette index with our background colour
Map_1_1_Palette_End:

Map_1_1_Tiles:
.include "maps/level1_1/level1_1_tiles.inc"
Map_1_1_Tiles_End:

Music_1_n:
.incbin "music/level_1_1.psg"

Map_1_n_MonsterTiles:
.include "gfx/monsters_tiles.inc"
Map_1_n_MonsterTiles_End:

Map_1_1_BlockMapData:
.include "maps/level1_1/level1_1_blockMap.inc"

Map_1_1_HitFlags:
.include "maps/level1_1/level1_1_hitflags.inc"

Map_1_1_Objects:
.db 9 ; number of objects on map

.db OBJ_SNAIL ; type
.db OBJ_FLAG_DIR_LEFT
.dw 360       ; x pos
.dw 272       ; y pos
.dw 768         ; misc data
.dw 768         ; counter
.db $42       ; starting tilenumber

.db OBJ_SNAIL ; type
.db OBJ_FLAG_DIR_RIGHT
.dw 260       ; x pos
.dw 272       ; y pos
.dw 768         ; misc data
.dw 768         ; counter
.db $42       ; starting tilenumber

.db OBJ_BIRD ; type
.db OBJ_FLAG_DIR_RIGHT | OBJ_FLAG_LONG
.dw 624       ; x pos
.dw 224       ; y pos
.dw 768         ; misc data
.dw 768         ; counter
.db $54       ; starting tilenumber

.db OBJ_SNAIL ; type
.db OBJ_FLAG_DIR_LEFT
.dw 624      ; x pos
.dw 256      ; y pos
.dw 240         ; misc data
.dw 240         ; counter
.db $42       ; starting tilenumber

.db OBJ_JUMPING_FISH ; type
.db OBJ_FLAG_DIR_RIGHT
.dw 1824       ; x pos
.dw 320       ; y pos
.dw 320|(2<<14)         ; misc data
.dw 33         ; counter
.db $34       ; starting tilenumber

.db OBJ_JUMPING_FISH ; type
.db OBJ_FLAG_DIR_RIGHT
.dw 1908       ; x pos
.dw 324       ; y pos
.dw 324|(3<<14)         ; misc data
.dw 0         ; counter
.db $34       ; starting tilenumber

.db OBJ_JUMPING_FISH ; type
.db OBJ_FLAG_DIR_RIGHT
.dw 1902       ; x pos
.dw 324       ; y pos
.dw 324|(1<<14)         ; misc data
.dw 40         ; counter
.db $34       ; starting tilenumber

.db OBJ_NEW_LIFE ; type
.db OBJ_FLAG_STATIC
.dw 1440      ; x pos
.dw 240       ; y pos
.dw 240         ; misc data
.dw 240         ; counter
.db $8       ; starting tilenumber

.db OBJ_LEVEL_END ; type
.db 0
.dw 296*8         ; x pos
.dw 32*8          ; y pos
.dw 1             ; misc data
.dw 0             ; counter
.db 0             ; starting tilenumber

.BANK 4 SLOT 2
.org $0000
Map_1_1_Blocks:
.include "maps/level1_1/level1_1_blocks.inc"

.BANK 5 SLOT 2
.ORG $0000
Map_1_2_Palette:
.include "maps/level1_2/level1_2_pal.inc"
.db $39 ; overwrite the first sprite palette index with our background colour
Map_1_2_Palette_End:

Map_1_2_Tiles:
.include "maps/level1_2/level1_2_tiles.inc"
Map_1_2_Tiles_End:

Map_1_2_BlockMapData:
.include "maps/level1_2/level1_2_blockMap.inc"


Map_1_2_HitFlags:
.include "maps/level1_2/level1_2_hitflags.inc"

Map_1_2_Objects:
.db 5 ; number of objects on map

.db OBJ_SNAIL ; type
.db OBJ_FLAG_DIR_LEFT
.dw 1104       ; x pos
.dw 400       ; y pos
.dw 768         ; misc data
.dw 768         ; counter
.db $42       ; starting tilenumber

.db OBJ_BIRD ; type
.db OBJ_FLAG_DIR_RIGHT | OBJ_FLAG_LONG
.dw 640       ; x pos
.dw 328       ; y pos
.dw 768         ; misc data
.dw 768         ; counter
.db $54       ; starting tilenumber

.db OBJ_SNAIL ; type
.db OBJ_FLAG_DIR_RIGHT
.dw 3080      ; x pos
.dw 400       ; y pos
.dw 240         ; misc data
.dw 240         ; counter
.db $42       ; starting tilenumber

.db OBJ_NEW_LIFE ; type
.db OBJ_FLAG_STATIC
.dw 2448      ; x pos
.dw 336       ; y pos
.dw 240         ; misc data
.dw 240         ; counter
.db $8       ; starting tilenumber

.db OBJ_LEVEL_END ; type
.db 0
.dw 607*8         ; x pos
.dw 32*8          ; y pos
.dw 1             ; misc data
.dw 0             ; counter
.db 0             ; starting tilenumber

; game maps

.BANK 6 SLOT 2
.org $0000
Map_1_2_Blocks:
.include "maps/level1_2/level1_2_blocks.inc"

.BANK 7 SLOT 2
.org $0000
.include "maps/level1_2/level1_2_blocks1.inc"

.BANK 8 SLOT 2
.org $0000
MonkeyladTiles:
.include "gfx/monkeylad_tiles.inc"
MonkeyLadTilesEnd:

SpritePalette:
.include "gfx/monkeylad_pal.inc"
SpritePaletteEnd:

;.BANK 9 SLOT 2
;.org $0000



;.ends
