.define SPRITE_CLEAR_FLAG $d0

;; Write zero to 16KB of VRAM
ClearVRAM:
    push af
    push bc
    ld a,$00
    out (VDPStatusPort),a
    ld a,$40
    out (VDPStatusPort),a
    ; 2. Output 16KB of zeroes
    ld bc, $4000    ; Counter for 16KB of VRAM
    -:
        ld a,$00    ; Value to write
        out (VDPDataPort),a ; Output to VRAM address, which is auto-incremented after each write
        dec bc
        ld a,b
        or c
        jp nz,-

    pop bc
    pop af
ret

;;Set palette
;hl palette address
;b palette size
;trashes bc
LoadPalette:
    push af
    push hl
    ld a,$00
    out (VDPStatusPort),a
    ld a,$c0
    out (VDPStatusPort),a
    ; 2. Output colour data
    ld c,VDPDataPort
    otir
    pop hl
    pop af
ret

;;Set palette entries at CRAM offset.
;hl palette address
;b palette size
;c palette starting index offset
;trashes bc
LoadPaletteWithOffset:
    push af
    push hl
    ld a, c
    out (VDPStatusPort),a
    ld a,$c0
    out (VDPStatusPort),a
    ; 2. Output colour data
    ld c,VDPDataPort
    otir
    pop hl
    pop af
ret

;trashes AF
ScreenOn:
    ; Turn screen on
    ld a,%11100010
    ;      |||| |`- Zoomed sprites -> 16x16 pixels
    ;      |||| `-- Doubled sprites -> 2 tiles per sprite, 8x16
    ;      |||`---- 30 row/240 line mode
    ;      ||`----- 28 row/224 line mode
    ;      |`------ VBlank interrupts
    ;      `------- Enable display
    out (VDPStatusPort),a
    ld a,$81 ;reg 1
    out (VDPStatusPort),a
ret

;trashes AF
ScreenOff:
    ; Turn screen on
    ld a,%10100000
    ;      |||| |`- Zoomed sprites -> 16x16 pixels
    ;      |||| `-- Doubled sprites -> 2 tiles per sprite, 8x16
    ;      |||`---- 30 row/240 line mode
    ;      ||`----- 28 row/224 line mode
    ;      |`------ VBlank interrupts
    ;      `------- Enable display
    out (VDPStatusPort),a
    ld a,$81 ;reg 1
    out (VDPStatusPort),a
ret


DetectTVType:
; Returns a=0 for NTSC, a=1 for PAL
; uses a, hl, de
    di             ; disable interrupts
    ld a,%01100000 ; set VDP such that the screen is on
    out ($bf),a    ; with VBlank interrupts enabled
    ld a,$81
    out ($bf),a
    ld hl,$0000    ; init counter
-:  in a,($bf)     ; get VDP status
    or a           ; inspect
    jp p,-         ; loop until frame interrupt flag is set
-:  in a,($bf)     ; do the same again, in case we were unlucky and came in just
    or a           ;   before the start of the VBlank with the flag already set
    jp p,-
    ; the VDP must now be at the start of the VBlank
-:  inc hl         ; (6 cycles) increment counter until interrupt flag comes on again
    in a,($bf)     ; (11 cycles)
    or a           ; (4 cycles)
    jp p,-         ; (10 cycles)
    xor a          ; reset carry flag, also set a=0
    ld de,2048     ; see if hl is more or less than 2048
    sbc hl,de
    ret c          ; if less, return a=0
    ld a,1
    ret            ; if more or equal, return a=1

;==============================================================
; Load tiles
;==============================================================
; 1. Set VRAM write address to tile index 0
; by outputting $4000 ORed with $0000
; hl tile data
; bc tile data length
; trashes af
LoadTiles:
    ld a,$00
    out (VDPStatusPort),a
    ld a,$40
    out (VDPStatusPort),a
    ; 2. Output tile data
-:
    ; Output data byte then three zeroes, because our tile data is 1 bit
    ; and must be increased to 4 bit
    ld a,(hl)        ; Get data byte
    out (VDPDataPort),a
    inc hl           ; Add one to hl so it points to the next data byte
    dec bc
    ld a,b
    or c
    jp nz,-
ret

;==============================================================
; Load sprite tiles
;==============================================================
; 1. Set VRAM write address to tile index 0
; by outputting $4000 ORed with $0000
; hl tile data
; bc tile data length
; trashes af
LoadSpriteTiles:
  ld a,$00
  out (VDPStatusPort),a
  ld a,$20|$40
  out (VDPStatusPort),a
  ; 2. Output tile data
-:
  ; Output data byte then three zeroes, because our tile data is 1 bit
  ; and must be increased to 4 bit
  ld a,(hl)        ; Get data byte
  out (VDPDataPort),a
  inc hl           ; Add one to hl so it points to the next data byte
  dec bc
  ld a,b
  or c
  jp nz,-
ret

;==============================================================
; Load tiles at an address in VRAM
;==============================================================
; 1. Set VRAM write address to tile index 0
; by outputting $4000 ORed with $0000
; hl tile data
; bc tile data length
; de address to load to in VRAM
; trashes af, bc
LoadTilesToAddress:
  ld a, e
  out (VDPStatusPort),a
  ld a, d
  or $40
  out (VDPStatusPort),a
  ; 2. Output tile data
-:
  ; Output data byte then three zeroes, because our tile data is 1 bit
  ; and must be increased to 4 bit
  ld a,(hl)        ; Get data byte
  out (VDPDataPort),a
  inc hl           ; Add one to hl so it points to the next data byte
  dec bc
  ld a,b
  or c
  jp nz,-
ret

;==============================================================
; Write text to name table
;==============================================================
; 1. Set VRAM write address to name table index 0
; by outputting $4000 ORed with $3800+0
; hl tile data
; bc tile data length
; trashes af
LoadTilemap:
ld a,$00
out (VDPStatusPort),a
ld a,$38|$40
out (VDPStatusPort),a
; 2. Output tilemap data
-:
ld a,(hl)    ; Get data byte
out (VDPDataPort),a
inc hl       ; Point to next letter
dec bc
ld a,b
or c
jp nz,-
ret


;Initialise the Sprite Attribute Table
;
;set the 1st sprite to cleared (y = $d0) to force the end of the sprite list
; 
;trashes af, b, l
InitSprites:  

  ld bc, 0
  ld l, SPRITE_CLEAR_FLAG
  call InitSprite
ret

; b = end of sprite index
MarkEndOfSprites:  
   ;b = index for end sprite.
   ld c, 0
   ld h, 0
   ld l, SPRITE_CLEAR_FLAG
   call InitSprite
ret

;b = sprite number
;c = tile number
;h = x pos
;l = y pos

; trashes af
InitSprite:
  ld a, b
  add a, a
  add a, $81 ; tile num
  out (VDPStatusPort),a
  ld a, $40|$3f
  out (VDPStatusPort),a
  ld a, c; tile num
  out (VDPDataPort), a
    
  ld a, b 
  add a, a
  add a, $80 ; xpos
  out (VDPStatusPort),a
  ld a, $40|$3f
  out (VDPStatusPort),a
  ld a, h; x position
  out (VDPDataPort), a

  ld a, b ; ypos
  out (VDPStatusPort),a
  ld a, $40|$3f
  out (VDPStatusPort),a
  ld a, l; y position
  out (VDPDataPort), a 
ret


;hl tilemap start offset
;bc tile data 
TileMapSetTile:
    push af

    ld a,l
    out (VDPStatusPort),a
    ld a,h
    or $40
    out (VDPStatusPort),a
    ld a, c
    out (VDPDataPort),a
    ld a, b
    out (VDPDataPort),a

    pop af
ret

;writes a tile to the tilemap from (hl) to address starting at de
;de tilemap start offset
;(hl) tile data 
TileMapSetTileFromHL:
   push af
   push hl
   
   ld a,e
   out (VDPStatusPort),a
   ld a,d
   or $40
   out (VDPStatusPort),a
        
   push bc
   ld c, VDPDataPort
   outi
   outi
   pop bc

   pop hl
   pop af
ret

;Copy tilenum data from de to tilemap b times starting from (hl)
;hl tilemap start offset
;b  number of tiles
;de tile data  
TileMapSetToTile:
    push af
    push bc

    ld a,l
    out (VDPStatusPort),a
    ld a,h
    or $40
    out (VDPStatusPort),a
    ; 2. Output tilemap data
    -:
        ld a, e
        out (VDPDataPort),a
        ld a, d
        out (VDPDataPort),a
        xor a
        dec b
        or b
    jp nz,-

    pop bc
    pop af
ret


;Copy tilemap data starting at DE to VRAM at (hl)
;hl tilemap start offset
;c  number of tiles
;de start of tile data to copy
TileMapSetTiles:
    push af
    push bc
    push de 

    ld a,l
    out (VDPStatusPort),a
    ld a,h
    or $40
    out (VDPStatusPort),a
    ; 2. Output tilemap data
    sla c ; c = c * 2 as tilemap entries are words.
    -:
        ld a, (de)
        out (VDPDataPort),a
        inc de
        xor a
        dec c
        or c
    jp nz,-

    pop de 
    pop bc
    pop af
ret

; b = x pos on screen
; c = y pos on screen
; tile returned in hl
;trashes a, bc, de, hl
ClearTileFromTileMap:
   ld a, (screenYPos)
   add a, c
   cp 28
   jp c, +
   ;begin  -- screenXPos + c >= 28
   add a, -28
   ;endif 
+:
   ld c, a
   
   ld a, (screenStartTileXPos)
;   ld c, a
;   ld a, (xScrollDir)
;   add a, c
   add a, b
   and %11111 ; a = (screenStartTileXPos + b) % 32
   ld b, a
   jp ClearMapTile
;ret

; Clears a tile from the tilemap
; b = tilemap x coord
; c = tilemap y coord
; trashes hl, bc, de
ClearMapTile:
   ;hl = (c * 32 + b) * 2
   ld h, 0
   ld l, c
   .rept 5
   add hl, hl
   .endr
   ld c, b
   ld b, 0
   add hl, bc
   add hl, hl

   ld de, TILEMAP_START_ADDRESS
   add hl, de
   ld b, 0
   ld c, 0 ;bc = 0 tilenumber
   call TileMapSetTile
ret

;Gets a tile from the tilemap.
.macro TileMapReadTile
; hl = address of tile to read.
; returns tilenum in hl
; trashes a
    ld a,l
    out (VDPStatusPort),a
    ld a,h
    out (VDPStatusPort),a
    push ix
    pop ix
    in a, (VDPDataPort)
    ld l, a
    ;waste some time before next read byte is ready. need 29 cycles
    ld a, 0
    ld a, 0
    nop
    nop
    nop
    in a, (VDPDataPort)
    ld h, a
.endm

HideColumnZero:
   ld a, $24
   out (VDPStatusPort),a
   nop
   nop
   ld a, $80
   out (VDPStatusPort),a
   nop
   nop
   ret

; Set the overscan / background colour index.
; a colour index from sprite palette.
SetOverscanBGColour:
   ;ld a, $24
   out (VDPStatusPort),a
   nop
   nop
   ld a, $87
   out (VDPStatusPort),a
   nop
   nop
ret

;Adds a new sprite to the SAT in RAM
.macro SpriteAdd
;b = x pos
;c = y pos
;a = tile number
;trashes a, de, hl
push af
ld a, (SpriteCount)
ld d, 0
ld e, a
ld hl, SpriteAttributeTableY
add hl, de
ld (hl), c

add a, a
ld e, a

ld hl, SpriteAttributeTableXN
add hl, de   ; hl = SpriteAttributeTableXN + SpriteCount * 2
pop af
ld (hl), b
inc hl
ld (hl), a

ld hl, SpriteCount
inc (hl)

.endm

.macro SpriteUpdateSAT
;trashes A
ld a, 1
ld (SpriteTableNeedsUpdate), a
.endm

SpritesWriteToVRAM:
   ; write Y position data
   ld a, (SpriteStartOffset)
   out (VDPStatusPort),a
   ld a, $40|$3f
   out (VDPStatusPort),a
   ld hl, SpriteAttributeTableY
   ld c, VDPDataPort
   ld a, (SpriteCount)
   ld b, a
-:   
   outi
   jp nz, -

   ld a, SPRITE_CLEAR_FLAG
   out (VDPDataPort), a
   
   ; write X and SpriteNum data
   ld a, (SpriteStartOffset)
   add a, a
   add a, $80
   out (VDPStatusPort),a
   ld a, $40|$3f
   out (VDPStatusPort),a
   ld hl, SpriteAttributeTableXN
   ld c, VDPDataPort
   ld a, (SpriteCount)
   add a, a
   ld b, a
-:   
   outi
   jp nz, -
ret
