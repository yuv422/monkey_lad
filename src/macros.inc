;Gets the address for the requested block. Sets the bank for the block
; de = the requested block number
; returns the block address in de
; trashes a, hl
.macro GetBlockAddress
   ;de contains block number.
   ; check block number. If > 128 then inc bank address and dec block number by 128
   ld a, e ; check if de >= 128
   and $80
   or d
   
   ld h, d
   ld l, e

   jp z, + ; jump if de < 128
      ld a, (curLevelInfo.blockDataBank)
      ld e, a
      ; while hl >= 128 { hl = hl - 128; e++ } 
-:
      ld bc, -128
      
      add hl, bc ; hl = hl - 128
      
      inc e
      
      ld a, l ; check if hl >= 128
      and $80
      or h
      jp nz, - ; if hl still >= 128 then repeat hl -= 128
      ld a, e 
      jp ++
+:
   ld a, (curLevelInfo.blockDataBank)
++:

   HLx128
   ld ($ffff), a ; blockDataBank
   ld de, $8000
   
   add hl, de
   ld d, h
   ld e, l ; de = $8000 + blockNumber * 128
.endm

.macro PlayerSetDirectionRight
      ld a, PLAYER_DIR_RIGHT
      ld (player.dir), a
.endm

.macro PlayerSetDirectionLeft
      ld a, PLAYER_DIR_LEFT
      ld (player.dir), a
.endm

;.macro PlayerUpdateX
;   ; trashes hl, bc
;   ld hl, (player.xSubPixel)
;   ld b, h
;   ld c, l
;   ld hl, (player.vx)
;   add hl, bc
;   ld (player.xSubPixel), hl
;   ld a, h
;   or a
;   jp z, +
;   ld hl, (player.x)
;   ld b, 0
;   ld c, a
;   add hl, bc
;   ld (player.x), hl ; player.x = player.x + player.xSubPixel/256
;   xor a
;   ld (player.xSubPixel+1), a ; player.xSubPixel = player.xSubPixel % 256
;+:
;   
;.endm

.macro PlayerUpdateX
   ; trashes hl, bc
   ld hl, (player.xSubPixel)
   ld b, h
   ld c, l
   ld hl, (player.vx)
   add hl, bc
   ld c, h
   SignExtend_BC
   ld h, 0 ; player.xSubPixel %= 256
   ld (player.xSubPixel), hl
   ld hl, (player.x)
   ld (player.oldX), hl
   add hl, bc
   ld a, h
   or a
   jp p, +
;begin  --  player.x < 0
   ld hl, 0
;endif
+:
   ld (player.x), hl ; player.x = player.x + player.xSubPixel/256
   
   ld bc, (curLevelInfo.maxPlayerX)
   xor a
   sbc hl, bc
   jp m, +
;begin  --  player.x >= curLevelInfo.mapWidthInPixels
   ld (player.x), bc
;endif
+:

   
.endm

.macro PlayerUpdateY
   ; trashes hl, bc
   ld hl, (player.ySubPixel)
   ld b, h
   ld c, l
   ld hl, (player.vy)
   add hl, bc
   ld c, h
   SignExtend_BC
   ld h, 0 ; player.ySubPixel %= 256
   ld (player.ySubPixel), hl
   ld hl, (player.y)
   add hl, bc

   ld (player.y), hl ; player.y = player.y + player.ySubPixel/256
.endm

