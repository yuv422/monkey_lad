;16/8 division
;http://wikiti.brandonw.net/index.php?title=Z80_Routines:Math:Division
;The following routine divides hl by c and places the quotient in hl and the remainder in a
div:
    push   bc

    xor    a
    ld     b,16

-:  add    hl,hl
    rla
    cp     c
    jp     c, +
    sub    c
    inc    l

+:  djnz   -

    pop    bc

ret


;hl = h * e
mul:
    push   bc
    push   de

    ld     l,0
    ld     d,l

    ld     b,8

-:  add    hl,hl
    jp     nc,+
    add    hl,de
+:  djnz   -

    pop    de
    pop    bc
ret


;;--------------------------------------------------
;; Binary to BCD conversion
;;
;; Converts a 16-bit unsigned integer into a 6-digit
;; BCD number. 1181 Tcycles
;;
;; input: HL = unsigned integer to convert
;; output: C:HL = 6-digit BCD number
;; destroys: A,F,B,C,D,E,H,L
;;--------------------------------------------------
Bin2Bcd:
    ld bc, 16*256+0 ; handle 16 bits, one bit per iteration
    ld de, 0
-:
    add hl, hl
    ld a, e
    adc a, a
    daa
    ld e, a
    ld a, d
    adc a, a
    daa
    ld d, a
    ld a, c
    adc a, a
    daa
    ld c, a
    djnz - 
    ex de, hl
ret

;trashes A
.macro HLx64
.rept 6
   add hl, hl
.endr

;FIXME this is HL = L * 64
;   xor a
;   srl l
;   rra
;   srl l
;   rra
;   ld h, l
;   ld l, a
.endm

;trashes A
.macro DE_EQUALS_Ex64
   xor a
   srl e
   rra
   srl e
   rra
   ld d, e
   ld e, a
.endm

.macro HLx128
   srl l
   ld h, l
   ld l, 0
   rr l
.endm

.macro HLx8
.rept 3
   add hl, hl
.endr
.endm

.macro BCdiv8
   srl b
   rr c
   srl b
   rr c
   srl b
   rr c
.endm

.macro DEdiv8
   srl d
   rr e
   srl d
   rr e
   srl d
   rr e
.endm

.macro HLdiv8
   srl h
   rr l
   srl h
   rr l
   srl h
   rr l
.endm

.macro HLdiv64
   ; Trashed: A
   xor a
   add hl, hl
   rla
   add hl, hl
   rla
   ld l, h
   ld h, a
.endm

;Can't use this at the moment as Z80Editor doesn't support registers as macro call arguments. :(
;.macro SignExtend args msb, lsb
;; Extend lsb to msblsb
;   ld a, lsb
;   rlca            ; Move sign bit into carry flag
;   sbc a, a        ; A is now 0 or 11111111, depending on the carry
;   ld msb, a
;.endm

;; Sign extend macros

.macro SignExtend_BC
; Extend C to BC
   ld a, c
   rlca            ; Move sign bit into carry flag
   sbc a, a        ; A is now 0 or 11111111, depending on the carry
   ld b, a
.endm

.macro SignExtend_DE
; Extend E to DE
   ld a, e
   rlca            ; Move sign bit into carry flag
   sbc a, a        ; A is now 0 or 11111111, depending on the carry
   ld d, a
.endm

.macro SignExtend_HL
; Extend L to HL
   ld a, l
   rlca            ; Move sign bit into carry flag
   sbc a, a        ; A is now 0 or 11111111, depending on the carry
   ld h, a
.endm

.macro DE_add_A
   ; trashes A
   add a, e
   ld e, a
   ld a, 0
   adc a, d
   ld d, a
.endm

;negate HL
.macro NegHL
;trashes A
   xor a
   sub l
   ld l,a
   sbc a,a
   sub h
   ld h,a
.endm

;absolute value of HL
;trashes A
AbsHL:
   bit 7,h
   ret z
   NegHL
ret


