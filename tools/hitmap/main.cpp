#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

#define uint32 unsigned int
#define uint8 unsigned char

bool read_uint32(std::istream *is, uint32 *num) {
    std::string s;
    char c;
    while(is->get(c)) {
        if(c >= '0' && c <= '9') {
            s += c;
        } else {
            if (s.length() > 0) {
                break;
            }
        }
    }
    *num = (uint32)atol(s.c_str());

    return s.length() > 0 ? true : false;
}

int main(int argc, char **argv) {
    int total_tiles, num_bit_flags;

    if(argc < 4) {
        cout << "Usage: hitmap infile total_tiles num_bit_flags\n Use '-' to read data from stdin\n";
        return 1;
    }

    string filename = argv[1];
    istream *is;
    ifstream file;
    if(filename == "-") {
        is = &cin;
    } else {
        file.open(filename);
        is = &file;
    }

    total_tiles = atoi(argv[2]);
    num_bit_flags = atoi(argv[3]);

    uint8 hitFlags[256];
    memset(hitFlags, 0, sizeof(hitFlags));

    for(int i=0;i < num_bit_flags; i++) {
        for(int j=0;j < total_tiles; j++) {
            uint32 tile;
            if(!read_uint32(is, &tile))
                break;
            tile--;
            if(tile > 0) {
                hitFlags[j] |= (1 << i);
            }
        }
    }


    for(int i=0;i< total_tiles;i++) {
        if((i%16)==0) {
            cout << "\n.db";
        }
        cout << " " << (int)hitFlags[i];
    }

    return 0;
}

