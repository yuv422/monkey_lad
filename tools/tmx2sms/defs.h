//
// Created by efry on 28/09/2016.
//

#ifndef TMX2SMS_TYPES_H
#define TMX2SMS_TYPES_H

#define uint32 unsigned int
#define uint16 unsigned short

#define BLOCK_WIDTH 8
#define BLOCK_HEIGHT 8

#define NUM_TILES_IN_BLOCK (BLOCK_WIDTH * BLOCK_HEIGHT)

#endif //TMX2SMS_TYPES_H
