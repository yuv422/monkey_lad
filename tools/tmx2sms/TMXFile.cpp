//
// Created by efry on 23/10/2016.
//

#include <iostream>
#include <yxml.h>
#include "yxml.h"
#include "TMXFile.h"

#define XML_BUFSIZE 4096

Layer *TMXFile::loadLayer(const char *layerName)
{
    if(!tmxData) {
        return NULL;
    }

    Layer *layer =  new Layer(width, height);

    std::string attrValue;
    yxml_t *yxml = initXML();
    unsigned char *ptr = tmxData;
    bool isCorrectLayer = false;
    for(uint32 i=0; i < tmxDataLength; i++, ptr++) {
        yxml_ret_t r = yxml_parse(yxml, *ptr);
        switch (r) {
            case YXML_ATTRSTART :
                break;
            case YXML_ATTRVAL :
                    attrValue += *yxml->data;
                break;
            case YXML_ATTREND :
                if(!strcmp("layer", yxml->elem) && !strcmp("name", yxml->attr) && !strcmp(layerName, attrValue.c_str())) {
                    printf("found layer: %s\n", attrValue.c_str());
                    isCorrectLayer = true;
                }
                attrValue.clear();
                break;
            case YXML_CONTENT :
                if(isCorrectLayer) {
                    uint32 data;
                    if(readUint23(yxml->data[0], &data)) {
                        layer->addTileValue(data);
                    }
                }
                break;
            case YXML_ELEMSTART :
                if(isCorrectLayer && strcmp("data", yxml->elem) != 0) {
                    //we've reached the end of the layer data so return.
                    free(yxml);
                    return layer;
                }
            default : break;
        }
        if(r < 0) {
            printf("XML parse error. %d", r);
            free(yxml);
            return layer;
        }
    }
    free(yxml);
    return layer;
}

bool TMXFile::readUint23(char c, uint32 *num)
{
        if(c >= '0' && c <= '9') {
            numericBuffer += c;
            return false;
        } else {
            if (numericBuffer.length() == 0) {
                return false;
            }
        }

    *num = (uint32)atol(numericBuffer.c_str());
    numericBuffer.clear();
    return true;
}

yxml_t *TMXFile::initXML() {
    yxml_t *x = (yxml_t *)malloc(sizeof(yxml_t) + XML_BUFSIZE);
    yxml_init(x, x+1, XML_BUFSIZE);
    return x;
}

bool TMXFile::loadFile(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    if(!f) {
        printf("Failed to open %s\n", filename);
        return false;
    }

    fseek(f, 0, SEEK_END);
    tmxDataLength = ftell(f);
    rewind(f);

    if(tmxDataLength == 0) {
        printf("Empty file\n");
        return false;
    }

    tmxData = (unsigned char *)malloc(tmxDataLength);
    if(!tmxData) {
        printf("Error allocating memory (Bytes: %d) for tmx file.\n", tmxDataLength);
        return false;
    }

    size_t bytesRead = 0;
    for(; bytesRead < tmxDataLength; ) {
        size_t i = fread(tmxData, 1, tmxDataLength - bytesRead, f);
        if(!i) {
            break;
        }
        bytesRead +=i;
    }

    if(bytesRead != tmxDataLength) {
        printf("Error reading file %s\n", filename);
        return false;
    }

    if(!readMapAttr("width", &width)) {
        printf("Error reading map width\n");
        return false;
    }

    if(!readMapAttr("height", &height)) {
        printf("Error reading map height\n");
        return false;
    }

    if(width % 8 != 0 && height % 8 != 0) {
        printf("Error map width and height must be multiples of 8\n");
    }

    return true;
}

bool TMXFile::readMapAttr(const char *attr, uint32 *value) {
    yxml_t *yxml = initXML();
    std::string valStr;
    unsigned char *ptr = tmxData;

    for(uint32 i=0; i < tmxDataLength; i++, ptr++) {
        yxml_ret_t r = yxml_parse(yxml, *ptr);
        if (r == YXML_ATTRVAL || r == YXML_ATTREND) {
            if(!strcmp("map", yxml->elem) && !strcmp(yxml->attr, attr)) {
                if(r == YXML_ATTRVAL) {
                    valStr += yxml->data[0];
                } else {
                    *value = (uint32)atol(valStr.c_str());
                    free(yxml);
                    return true;
                }
            }
        }
    }
    free(yxml);
    return false;
}