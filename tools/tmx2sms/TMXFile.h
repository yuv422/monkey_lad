//
// Created by efry on 23/10/2016.
//

#ifndef TMX2SMS_TMXFILE_H
#define TMX2SMS_TMXFILE_H


#include <string>
#include "yxml.h"
#include "defs.h"

class Layer {
public:
    uint32 w;
    uint32 h;
    uint32 *data;
    uint32 elementCount;

    Layer(uint32 width, uint32 height) {
        w = width;
        h = height;
        elementCount = 0;
        data = (uint32 *)malloc(sizeof(uint32) * w * h);
        bzero(data, sizeof(uint32) * w * h);
    }

    ~Layer() {
        if(data) {
            free(data);
        }
    }

    void addTileValue(uint32 value) {
        if(elementCount < w * h) {
            data[elementCount++] = value;
        }
    }
};

class TMXFile
{
private:
    unsigned char *tmxData;
    int tmxDataLength;
    std::string numericBuffer;
    uint32 width;
    uint32 height;

public:
    TMXFile() {
        tmxData = NULL;
        width = 0;
        height = 0;
    }

    ~TMXFile() {
        if(tmxData) {
            free(tmxData);
        }
    }
    bool loadFile(const char *filename);
    Layer *loadLayer(const char *layerName);

    uint32 getWidth() { return width; }

    uint32 getHeight() { return height; }

private:
    bool readUint23(char c, uint32 *num);

    yxml_t *initXML();

    bool readMapAttr(const char *attr, uint32 *value);
};


#endif //TMX2SMS_TMXFILE_H
