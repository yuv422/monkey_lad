#ifndef TMX2SMS_MAP_H
#define TMX2SMS_MAP_H

#include <iostream>
#include <vector>
#include <assert.h>
#include "defs.h"
#include "Block.h"
#include "TMXFile.h"

using namespace std;

class Map {
public:
    int w;
    int h;
    Layer *layer[5];
    vector<Block *> blockMap;
    vector<Block *> blocks;
    int numItemBlocks;
    uint16 numberOfItemTiles;

private:

public:
    Map(TMXFile *file, uint16 itemTileCount) {
        w = file->getWidth();
        h = file->getHeight();
        numberOfItemTiles = itemTileCount;
        numItemBlocks = 0;

        layer[0] = file->loadLayer("Bottom");
        layer[1] = file->loadLayer("front");
        layer[2] = file->loadLayer("camera");
        layer[3] = file->loadLayer("passable");
        layer[4] = file->loadLayer("unused");
    }

    ~Map() {
        for(int i=0;i<5;i++) {
            free(layer[i]);
        }
    }

    void process();

private:
    void createBlock(int x, int y);
    Block *findDuplicate(Block *block);
    bool isItemBlock(Block *block);
    void addBlock(Block *pBlock);
    void addItemBlock(Block *pBlock);
    uint16 getTilenumber(uint16 x, uint16 y);


};


#endif //TMX2SMS_MAP_H
