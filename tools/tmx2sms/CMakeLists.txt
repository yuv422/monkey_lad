cmake_minimum_required(VERSION 3.3)
project(tmx2sms)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES defs.h main.cpp Map.cpp Map.h Block.h yxml/yxml.c yxml/yxml.h TMXFile.cpp TMXFile.h)

include_directories(yxml)

add_executable(tmx2sms ${SOURCE_FILES})