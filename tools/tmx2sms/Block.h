//
// Created by efry on 28/09/2016.
//

#ifndef TMX2SMS_BLOCK_H
#define TMX2SMS_BLOCK_H

#include "defs.h"

class Block {
public:
    uint16 id;
    uint16 *data;
    bool is_duplicate;
    Block *original_block;

    Block() {
        id = 0;
        data = (uint16 *)malloc(sizeof(uint16) * NUM_TILES_IN_BLOCK);
        assert(data);
        is_duplicate = false;
        original_block = NULL;
    }


    virtual ~Block() {
        free(data);
    }
};

#endif //TMX2SMS_BLOCK_H
