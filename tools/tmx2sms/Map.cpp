//
// Created by efry on 28/09/2016.
//

#include "Map.h"

#define TMX_FLIP_X_FLAG 0x80000000
#define TMX_FLIP_Y_FLAG 0x40000000

#define TILEMAP_H_FLIP_FLAG 0x200
#define TILEMAP_V_FLIP_FLAG 0x400
#define TILEMAP_PALETTE_FLAG 0x800
#define TILEMAP_PRIORITY_FLAG 0x1000
#define TILEMAP_U1_FLAG 0x2000
#define TILEMAP_U2_FLAG 0x4000
#define TILEMAP_U3_FLAG 0x8000

void Map::addBlock(Block *pBlock) {
    if (!pBlock->is_duplicate) {
        pBlock->id = blocks.size();
        blocks.push_back(pBlock);
    }
    blockMap.push_back(pBlock);
}

void Map::addItemBlock(Block *pBlock) {
    blocks.insert(blocks.begin(), pBlock);
    int i = 0;
    for(vector<Block *>::iterator itr = blocks.begin();itr != blocks.end(); ++itr, i++) {
        (*itr)->id = i;
    }

    blockMap.push_back(pBlock);
    numItemBlocks++;
}

uint16 Map::getTilenumber(uint16 x, uint16 y) {
    uint32 tmxTileNum = layer[0]->data[y * w + x];
    uint16 tileNum = (uint16)(tmxTileNum & 0xffff);

    tileNum--;

    if((tmxTileNum & TMX_FLIP_X_FLAG) == TMX_FLIP_X_FLAG) {
        tileNum |= TILEMAP_H_FLIP_FLAG;
    }

    if((tmxTileNum & TMX_FLIP_Y_FLAG) == TMX_FLIP_Y_FLAG) {
        tileNum |= TILEMAP_V_FLIP_FLAG;
    }

    if ( layer[1]->data[y * w + x] > 1 ) { // in front tile (priority)
        tileNum |= TILEMAP_PRIORITY_FLAG;
    }

    if ( layer[2]->data[y * w + x] > 1 ) { // U1 flag
        tileNum |= TILEMAP_U1_FLAG;
    }

    if ( layer[3]->data[y * w + x] > 1 ) { // U2 flag
        tileNum |= TILEMAP_U2_FLAG;
    }

    if ( layer[4]->data[y * w + x] > 1 ) { // U3 flag
        tileNum |= TILEMAP_U3_FLAG;
    }

    return tileNum;
}

Block *Map::findDuplicate(Block *block) {
    int size = (int) blocks.size();

    for (int i = 0; i < size; i++) {
        Block *t = blocks.at(i);
        int count = 0;
        for (; count < NUM_TILES_IN_BLOCK; count++) {
            if (t->data[count] != block->data[count]) {
                break;
            }
        }
        if (count == NUM_TILES_IN_BLOCK) {
            return t;
        }
    }

    return NULL;
}

bool Map::isItemBlock(Block *block) {
    for(int i=0;i<NUM_TILES_IN_BLOCK;i++) {
        if(block->data[i] != 0 && block->data[i] <= numberOfItemTiles) {
            return true;
        }
    }
    return false;
}

void Map::createBlock(int x, int y) {
    Block *block = new Block();

    uint16 *tile_ptr = block->data;
    for (int i = y; i < y + BLOCK_HEIGHT; i++) {
        for (int j = x; j < x + BLOCK_WIDTH; j++) {
            tile_ptr[0] = getTilenumber(j, i);
            tile_ptr++;
        }
    }

    if(isItemBlock(block)) {
        addItemBlock(block);
    } else {
        Block *origBlock = findDuplicate(block);
        if(origBlock) {
            block->is_duplicate = true;
            block->original_block = origBlock;
        }

        addBlock(block);
    }
}

void Map::process() {
    for (int y = 0; y < h; y += BLOCK_HEIGHT) {
        for (int x = 0; x < w; x += BLOCK_WIDTH) {
            createBlock(x, y);
        }
    }
}


