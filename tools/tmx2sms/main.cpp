#include <iostream>
#include <vector>
#include <fstream>

#include "defs.h"
#include "Map.h"
#include "TMXFile.h"

using namespace std;

#define OUTPUT_NUM_COLS 20
#define BLOCKS_PER_FILE 128


void write_uint16(ostream &out, uint16 num) {
    char buffer[6]; // '$FFFF\0'

    sprintf(buffer, "$%04X", num);

    out << buffer;
}

void write_block_data(ostream &out, Block *block) {
    out << "\n; Block " << block->id;
    for(int i=0;i < NUM_TILES_IN_BLOCK; i++) {
        if (i % BLOCK_WIDTH == 0) {
            out << "\n.dw ";
        }
        out << " ";
        write_uint16(out, block->data[i]);
    }
    out << "\n";
}

void write_sms_map_config(Map *map, string filename) {
    ofstream config;
    config.open(filename + "_config.inc");
    config << ".dw " << map->w << " ; map width\n";
    config << ".dw " << map->h << " ; map height\n";
    config << ".db " << map->w << " >> 3 ; mapWidthInBlocks\n";
    config << ".dw " << map->w << " * 8 - 24 ; maxPlayerX\n";
    config << ".db " << map->numberOfItemTiles << " ; numItemTiles\n";
    config << ".db " << map->numItemBlocks << " ; numItemBlocks\n";

    config.close();
}

void write_sms_data(Map *map, string filename) {
    ofstream blockmap;
    blockmap.open (filename + "_blockMap.inc");
    blockmap << "; Number of unique blocks: " << map->blocks.size() << " total bytes: $" << std::hex << (4 + map->blocks.size() * 64 * 2 + map->blockMap.size() * 2) << std::dec << "\n";
    blockmap << "; Number of item blocks: " << map->numItemBlocks << "\n";
    blockmap << "; .dw " << map->w << " ; map width\n";
    blockmap << "; .dw " << map->h << " ; map width\n\n\n";

    blockmap << "; block map.\n\n";

    for(int i=0; i<map->blockMap.size(); i++) {
        if (i % OUTPUT_NUM_COLS == 0) {
            blockmap << "\n.dw ";
        }
        blockmap << " ";
        uint16 id = map->blockMap[i]->is_duplicate ? map->blockMap[i]->original_block->id :  map->blockMap[i]->id;
        write_uint16(blockmap, id);
    }
    blockmap.close();

    // Write block data out in groups of 128 blocks
    for(int i=0;i*BLOCKS_PER_FILE < map->blocks.size(); i++) {
        ofstream blockdata;
        blockdata.open(filename + "_blocks" + (i == 0 ? "" : to_string(i)) + ".inc");
        for(int j=i*BLOCKS_PER_FILE;j < map->blocks.size() && j < (i+1) * BLOCKS_PER_FILE;j++)
        {
            Block *block = map->blocks.at(j);
            write_block_data(blockdata, block);
        }
        blockdata.close();
    }

    write_sms_map_config(map, filename);
}

std::string trimTmxExtension(const char *filename) {
    const char ext[] = ".tmx";

    if(strlen(filename) > strlen(ext) && !strcmp(&filename[strlen(filename) - strlen(ext)], ext)) {
        return string(filename).substr(0, strlen(filename) - strlen(ext));
    }

    return string(filename);
}

int main(int argc, char **argv) {
    if(argc < 2) {
        cout << "Usage: tmx2sms infile <numberOfItemTiles>";
        return 1;
    }

    uint16 numberOfItemTiles = argc > 2 ? (uint16)atoi(argv[2]) : (uint16)0;

    TMXFile tmx;
    if(!tmx.loadFile(argv[1])) {
        return 1;
    }

    Map *map = new Map(&tmx, numberOfItemTiles);
    map->process();
    write_sms_data(map, trimTmxExtension(argv[1]));

    return 0;
}




